package com.cs.mawthuq.Rest;

import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.ChangePasswordResponce;
import com.cs.mawthuq.Models.ChatListResponse;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.Models.DashboardResponse;
import com.cs.mawthuq.Models.Drafts;
import com.cs.mawthuq.Models.HistoryResponse;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.Models.ResetpasswordResponce;
import com.cs.mawthuq.Models.SignUpResponse;
import com.cs.mawthuq.Models.TemplateList;
import com.cs.mawthuq.Models.UpdateProfile;
import com.cs.mawthuq.Models.ValidateMobileResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIInterface {

    @POST("api/UsersAPI/ValidateMobile")
    Call<ValidateMobileResponse> validateMobile(@Body RequestBody body);

    @POST("api/UsersAPI/UserSignUp")
    Call<SignUpResponse> userSignUp(@Body RequestBody body);

    @POST("api/UsersAPI/UserLogin")
    Call<SignUpResponse> userLogin(@Body RequestBody body);

    @POST("api/UsersAPI/ValidateForgotPwdMobile")
    Call<ValidateMobileResponse> forgotvalidateMobile(@Body RequestBody body);

    @POST("api/UsersAPI/SaveForgotPassword")
    Call<ResetpasswordResponce> userResetpassword(@Body RequestBody body);

    @POST("api/UsersAPI/UpdateProfile")
    Call<UpdateProfile> updateProfile(@Body RequestBody body);

    @POST("api/TemplateAPI/GetTemplateList")
    Call<TemplateList> getTemplateList(@Body RequestBody body);

    @POST("api/SignatureDocumentAPI/InsertSignatureDocument")
    Call<BasicResponse> insertSignatureDocument(@Body RequestBody body);

    @POST("api/SignatureDocumentAPI/SaveDraft")
    Call<BasicResponse> saveDraft(@Body RequestBody body);

    @POST("api/SignatureDocumentAPI/GetSignatureDocument")
    Call<InboxResponse> GetSignatureDocument(@Body RequestBody body);

    @POST("api/TemplateAPI/GetSignedUserDocDetailsById")
    Call<HistoryResponse> getHistoryDetails(@Body RequestBody body);

    @POST("api/SignatureDocumentAPI/GetDraft")
    Call<Drafts> GetDrafts(@Body RequestBody body);

    @POST("api/SignatureDocumentAPI/SendDraft")
    Call<BasicResponse> SendDraft(@Body RequestBody body);

    @POST("api/SignatureDocumentAPI/ReceiverUpdateSignatureDocument")
    Call<BasicResponse> ReceiverUpdateSignatureDocument(@Body RequestBody body);

    @POST("api/TemplateAPI/GetUserList")
    Call<Contacts> getUserList(@Body RequestBody body);

    @POST("api/SignatureDocumentAPI/GetDocChat")
    Call<ChatListResponse> getDocChat(@Body RequestBody body);

    @POST("api/SignatureDocumentAPI/SaveDocChat")
    Call<BasicResponse> saveDocChat(@Body RequestBody body);

    @POST("api/TemplateAPI/SaveFavouriteTemplate")
    Call<BasicResponse> saveFavouriteTemplate(@Body RequestBody body);

    @POST("api/UsersAPI/AddOrInviteUser")
    Call<BasicResponse> inviteUserToApp(@Body RequestBody body);

    @POST("api/UsersAPI/SaveChangePassword")
    Call<ChangePasswordResponce> userChangepassword(@Body RequestBody body);

    @POST("api/UsersAPI/UserDashboard")
    Call<DashboardResponse> userDashboard(@Body RequestBody body);
}
