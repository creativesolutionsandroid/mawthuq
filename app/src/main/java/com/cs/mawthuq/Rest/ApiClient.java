package com.cs.mawthuq.Rest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static final String BASE_URL = "http://csadms.com/SignatureApp/Services/"; // todo replace with live url
    private static Retrofit retrofit = null;

    public static Retrofit getClient(int timeOutMins) {
        if (retrofit == null) {

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .connectTimeout(timeOutMins, TimeUnit.MINUTES)
                    .readTimeout(2, TimeUnit.MINUTES);
            // add your other interceptors …

            httpClient.addInterceptor(new Interceptor() {
                                          @Override
                                          public Response intercept(Chain chain) throws IOException {

                                              Request original = chain.request();

                                              Request request = original.newBuilder()
                                                      .header("API_KEY", "27012021@Signature")
                                                      .method(original.method(), original.body())
                                                      .build();
                                              Response response = chain.proceed(request);

                                              return response;

                                          }
                                      }
            );

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }
}
