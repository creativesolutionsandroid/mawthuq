package com.cs.mawthuq.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Activitys.DashboardDraftSendActivity;
import com.cs.mawthuq.Activitys.DraftSendActivity;
import com.cs.mawthuq.Models.DashboardResponse;
import com.cs.mawthuq.Models.Drafts;
import com.cs.mawthuq.Models.PDFData;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DashboardDraftsAdapter extends RecyclerView.Adapter<DashboardDraftsAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private List<DashboardResponse.DraftDocument> templates = new ArrayList<>();

    public DashboardDraftsAdapter(Context context, List<DashboardResponse.DraftDocument> storeArrayList) {
        this.context = context;
        this.templates = storeArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_inbox_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (templates.get(position).getTemplateOption() == KeyConstants.TYPE_TEMPLATE) {
            holder.title.setText(templates.get(position).getTemplateNameEn());
        } else {
            String jsonString = templates.get(position).getSenderJSON();

            PDFData data = new PDFData();
            Gson gson = new Gson();
            data = gson.fromJson(jsonString, PDFData.class);

            holder.title.setText(data.getDocumentName());
        }

        holder.name.setText("To: " + templates.get(position).getFullName());
        String date = templates.get(position).getDraftdate();
        date = date.replace("T", " ");
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date newDate = spf.parse(date);
            spf = new SimpleDateFormat("dd/MM");
            date = spf.format(newDate);
            holder.date.setText(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (templates.get(position).getTemplateOption() == KeyConstants.TYPE_TEMPLATE) {
            holder.profilePic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_template));
        } else if (templates.get(position).getTemplateOption() == KeyConstants.TYPE_SCAN) {
            holder.profilePic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_scan));
        } else if (templates.get(position).getTemplateOption() == KeyConstants.TYPE_WRITE) {
            holder.profilePic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_write_new));
        }
    }

    @Override
    public int getItemCount() {
        return templates.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, name, date;
        LinearLayout layout;
        ImageView profilePic;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            profilePic = (ImageView) itemView.findViewById(R.id.profile_pic);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DashboardDraftSendActivity.class);
                    intent.putExtra("template", templates.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
