package com.cs.mawthuq.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Models.ChatListResponse;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.Constants;

import java.util.ArrayList;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ArrayList<ChatListResponse.Data> templates = new ArrayList<>();
    SharedPreferences userPrefs;
    int userId;

    public ChatListAdapter(Context context, ArrayList<ChatListResponse.Data> storeArrayList) {
        this.context = context;
        this.templates = storeArrayList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getInt(KEY_USERID, 0);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_chat, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (userId == templates.get(position).getUserId()) {
//        if (14 == templates.get(position).getUserId()) {
            holder.othersMessage.setVisibility(View.GONE);
            holder.yourMessage.setVisibility(View.VISIBLE);
            holder.yourMessage.setText(templates.get(position).getRemark());
        }
        else {
            holder.othersMessage.setVisibility(View.VISIBLE);
            holder.othersMessage.setText(templates.get(position).getRemark());
            holder.yourMessage.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return templates.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView othersMessage, yourMessage;

        public MyViewHolder(View itemView) {
            super(itemView);
            othersMessage = (TextView) itemView.findViewById(R.id.other_message);
            yourMessage = (TextView) itemView.findViewById(R.id.your_message);
        }
    }
}
