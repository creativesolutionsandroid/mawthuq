package com.cs.mawthuq.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Activitys.ContactsActivity;
import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.Models.LocalContacts;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class AddContactAdapter extends RecyclerView.Adapter<AddContactAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ArrayList<LocalContacts> templates = new ArrayList<>();
    SharedPreferences userPrefs;
    String inputStr;
    Activity activity;

    public AddContactAdapter(Context context, ArrayList<LocalContacts> storeArrayList, Activity activity) {
        this.context = context;
        this.templates = storeArrayList;
        this.activity = activity;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contacts_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.name.setText(templates.get(position).getName());
        holder.mobileNumber.setText(templates.get(position).getPhoneNumber());

        try {
            Bitmap bp = MediaStore.Images.Media
                    .getBitmap(context.getContentResolver(),
                            Uri.parse(templates.get(position).getImage()));
            holder.profilePic.setImageBitmap(bp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return templates.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, mobileNumber, invite;
        ImageView profilePic;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            mobileNumber = (TextView) itemView.findViewById(R.id.mobile_number);
            invite = (TextView) itemView.findViewById(R.id.btn_invite);

            profilePic = (ImageView) itemView.findViewById(R.id.profile_pic);
            invite.setVisibility(View.VISIBLE);

            invite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    prepareSignInJson(templates.get(getAdapterPosition()).getPhoneNumber());
                }
            });
        }
    }

    private void prepareSignInJson(String phoneNumber) {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 0));
            phoneNumber = phoneNumber.replace(" ", "");
            phoneNumber = phoneNumber.replace("+", "");
            if (!phoneNumber.startsWith("966")) {
                phoneNumber = "966" + phoneNumber;
            }
            parentObj.put("PhoneNumber", phoneNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        inputStr = parentObj.toString();
        new getContactsListApi().execute();
    }

    private class getContactsListApi extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<BasicResponse> call = apiService.inviteUserToApp(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    if (response.isSuccessful()) {
                        BasicResponse BasicResponse = response.body();
                        try {
                            Constants.showOneButtonAlertDialog(BasicResponse.MessageEn, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), activity);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
