package com.cs.mawthuq.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.cs.mawthuq.Models.ViewpagerResponce;
import com.cs.mawthuq.R;

import java.util.ArrayList;

public class WelcomeViewpagerAdapter extends PagerAdapter {


    LayoutInflater mLayoutInflater;
    ArrayList<ViewpagerResponce> bannersList;
    Context context;

    public WelcomeViewpagerAdapter(Context context, ArrayList<ViewpagerResponce> bannersList) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannersList = bannersList;
    }

    @Override
    public int getCount() {
        Log.d("TAG", "getCount: "+bannersList.size());
        return bannersList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {

        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_viewpager_adapter, container, false);

        TextView tittle = (TextView) itemView.findViewById(R.id.title);
        TextView desc = (TextView) itemView.findViewById(R.id.dsc);

        tittle.setText(bannersList.get(position).getTittle());
        desc.setText(bannersList.get(position).getDsc());

        container.addView(itemView);
        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
