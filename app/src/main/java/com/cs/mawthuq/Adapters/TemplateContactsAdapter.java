package com.cs.mawthuq.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Activitys.SenderTemplateSubmitActivity;
import com.cs.mawthuq.Listeners.ContactSelectedListener;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.Constants;

import java.util.ArrayList;

public class TemplateContactsAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<Contacts.UserList> contacts;
    Context context;
    ContactSelectedListener listener;
    int selectedPos;

    public TemplateContactsAdapter(Context context, ArrayList<Contacts.UserList> bannersList,
                                   ContactSelectedListener listener, int selectedPos) {
        this.context = context;
        this.listener = listener;
        this.selectedPos = selectedPos;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.contacts = bannersList;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 2.8f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_template_contact, container, false);

        TextView name = (TextView) itemView.findViewById(R.id.name);
        ImageView profilePic = (ImageView) itemView.findViewById(R.id.profile_pic);
        ImageView selected = (ImageView) itemView.findViewById(R.id.selected);
        RelativeLayout cardView = (RelativeLayout) itemView.findViewById(R.id.layout);

        name.setText(contacts.get(position).getFullName());

        Glide.with(context)
                .load(Constants.USERS_IMAGE_URL + contacts.get(position).getImageName())
                .placeholder(R.drawable.ic_profile_pic_round)
                .into(profilePic);

        if (selectedPos == position) {
            selected.setVisibility(View.VISIBLE);
        } else {
            selected.setVisibility(View.INVISIBLE);
        }

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onContactSelected(position);
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
