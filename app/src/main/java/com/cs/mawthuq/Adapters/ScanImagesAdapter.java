package com.cs.mawthuq.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Listeners.ScanImageListener;
import com.cs.mawthuq.R;

import java.util.ArrayList;

public class ScanImagesAdapter extends RecyclerView.Adapter<ScanImagesAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<Bitmap> images = new ArrayList<>();
    SharedPreferences userPrefs;
    String inputStr;
    private ScanImageListener listener;

    public ScanImagesAdapter(Context context, ArrayList<Bitmap> storeArrayList, Activity activity,
                             ScanImageListener favListener) {
        this.context = context;
        this.activity = activity;
        this.images = storeArrayList;
        this.listener = favListener;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_scan_imge, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (images.get(position) == null) {
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.upload_image));
        } else {
            holder.image.setImageBitmap(images.get(position));
//            Glide.with(context).load(images.get(position)).into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        RelativeLayout layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            layout = (RelativeLayout) itemView.findViewById(R.id.layout);

            image = (ImageView) itemView.findViewById(R.id.image);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onPlusClicked(getAdapterPosition());
                }
            });
        }
    }
}
