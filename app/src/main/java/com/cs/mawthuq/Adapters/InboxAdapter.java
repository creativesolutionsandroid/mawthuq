package com.cs.mawthuq.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Activitys.InboxTemplateEditActivity;
import com.cs.mawthuq.Activitys.SenderTemplateEditActivity;
import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.Models.PDFData;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ArrayList<InboxResponse.Data> templates = new ArrayList<>();

    public InboxAdapter(Context context, ArrayList<InboxResponse.Data> storeArrayList) {
        this.context = context;
        this.templates = storeArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_inbox_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (templates.get(position).getTemplateOption() == 1) {
            holder.title.setText(templates.get(position).getTemplateNameEn());
        }
        else {
            String jsonString = templates.get(position).getSendorJSON();

            PDFData data = new PDFData();
            Gson gson = new Gson();
            data = gson.fromJson(jsonString, PDFData.class);

            holder.title.setText(data.getDocumentName());
        }

        holder.name.setText("From: "+templates.get(position).getSenderFullName());
        String date = templates.get(position).getSentDate();
        date = date.replace("T", " ");
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date newDate = spf.parse(date);
            spf = new SimpleDateFormat("dd/MM");
            date = spf.format(newDate);
            holder.date.setText(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Glide.with(context)
                .load(Constants.USERS_IMAGE_URL + templates.get(position).getSenderImageName())
                .placeholder(R.drawable.ic_profile_pic_round)
                .into(holder.profilePic);
    }

    @Override
    public int getItemCount() {
        return templates.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, name, date;
        LinearLayout layout;
        ImageView profilePic;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            profilePic = (ImageView) itemView.findViewById(R.id.profile_pic);


            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, InboxTemplateEditActivity.class);
                    intent.putExtra("template", templates.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
