package com.cs.mawthuq.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.mawthuq.Activitys.SenderTemplateEditActivity;
import com.cs.mawthuq.Activitys.SenderTemplateSubmitActivity;
import com.cs.mawthuq.Activitys.VerifyOtpActivity;
import com.cs.mawthuq.Listeners.TemplateFavListener;
import com.cs.mawthuq.Models.TemplateList;
import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class TemplatesListAdapter extends RecyclerView.Adapter<TemplatesListAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<TemplateList.Template> templates = new ArrayList<>();
    SharedPreferences userPrefs;
    String inputStr;
    private TemplateFavListener favListener;

    public TemplatesListAdapter(Context context, ArrayList<TemplateList.Template> storeArrayList, Activity activity,
                                TemplateFavListener favListener) {
        this.context = context;
        this.activity = activity;
        this.templates = storeArrayList;
        this.favListener = favListener;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_template_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.title.setText(templates.get(position).getTemplateNameEn() + "("
                + templates.get(position).getTemplateTypeNameEn() + ")");
        holder.subTitle.setText(templates.get(position).getDescriptionEn());

        if (templates.get(position).getFavouriteTemplate()) {
            holder.fav.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
        } else {
            holder.fav.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_unselected));
        }

        holder.fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (templates.get(position).getFavouriteTemplate()) {
                    prepareVerifyMobileJson(templates.get(position).getTemplateId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return templates.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, subTitle;
        ImageView fav;
        RelativeLayout layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            subTitle = (TextView) itemView.findViewById(R.id.sub_title);
            layout = (RelativeLayout) itemView.findViewById(R.id.layout);

            fav = (ImageView) itemView.findViewById(R.id.fav);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, SenderTemplateEditActivity.class);
                    intent.putExtra("template", templates.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }

    private void prepareVerifyMobileJson(int templateId) {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("TemplateId", templateId);
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        inputStr = parentObj.toString();
        new insertFavApi().execute();
    }

    private class insertFavApi extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<BasicResponse> call = apiService.saveFavouriteTemplate(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    if (response.isSuccessful()) {
                        Constants.closeLoadingDialog();
                        BasicResponse BasicResponse = response.body();
                        try {
                            if (BasicResponse.isStatus()) {
                                favListener.onFavUpdated();
                            } else {
                                String failureResponse = BasicResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                        context.getResources().getString(R.string.ok), activity);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
