package com.cs.mawthuq.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Activitys.SenderTemplateEditActivity;
import com.cs.mawthuq.Listeners.TemplateFavListener;
import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.Models.TemplateList;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ArrayList<Contacts.UserList> templates = new ArrayList<>();

    public ContactsListAdapter(Context context, ArrayList<Contacts.UserList> storeArrayList) {
        this.context = context;
        this.templates = storeArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contacts_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.name.setText(templates.get(position).getFullName());
        holder.mobileNumber.setText(templates.get(position).getPhoneNumber());

        Glide.with(context)
                .load(Constants.USERS_IMAGE_URL + templates.get(position).getImageName())
                .placeholder(R.drawable.ic_profile_pic_round)
                .into(holder.profilePic);
    }

    @Override
    public int getItemCount() {
        return templates.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, mobileNumber;
        ImageView profilePic;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            mobileNumber = (TextView) itemView.findViewById(R.id.mobile_number);

            profilePic = (ImageView) itemView.findViewById(R.id.profile_pic);
        }
    }
}
