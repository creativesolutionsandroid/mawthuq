package com.cs.mawthuq.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Activitys.HistoryDetailsActivity;
import com.cs.mawthuq.Activitys.OutboxTemplateEditActivity;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.Models.PDFData;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ArrayList<InboxResponse.Data> templates = new ArrayList<>();
    SharedPreferences userPrefs;

    public HistoryAdapter(Context context, ArrayList<InboxResponse.Data> storeArrayList) {
        this.context = context;
        this.templates = storeArrayList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (templates.get(position).getTemplateOption() == 1) {
            holder.title.setText(templates.get(position).getTemplateNameEn());
        }
        else {
            String jsonString = templates.get(position).getSendorJSON();

            PDFData data = new PDFData();
            Gson gson = new Gson();
            data = gson.fromJson(jsonString, PDFData.class);

            holder.title.setText(data.getDocumentName());
        }

        holder.reference.setText(templates.get(position).getReferenceNo());
        if (!templates.get(position).getReceiverFullName().equals(userPrefs.getString(KeyConstants.KEY_USERNAME, ""))) {
            holder.name.setText("To: "+templates.get(position).getReceiverFullName());
            Glide.with(context)
                    .load(Constants.USERS_IMAGE_URL + templates.get(position).getReceiverImageName())
                    .placeholder(R.drawable.ic_profile_pic_round)
                    .into(holder.profilePic);
        }
        else {
            holder.name.setText("From: "+templates.get(position).getSenderFullName());
            Glide.with(context)
                    .load(Constants.USERS_IMAGE_URL + templates.get(position).getSenderImageName())
                    .placeholder(R.drawable.ic_profile_pic_round)
                    .into(holder.profilePic);
        }

        String date = templates.get(position).getSentDate();
        date = date.replace("T", " ");
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date newDate = spf.parse(date);
            spf = new SimpleDateFormat("dd/MM");
            date = spf.format(newDate);
            holder.date.setText(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return templates.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, reference, name, date;
        ImageView profilePic;
        LinearLayout layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            reference = (TextView) itemView.findViewById(R.id.reference);
            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            profilePic = (ImageView) itemView.findViewById(R.id.profile_pic);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, HistoryDetailsActivity.class);
                    intent.putExtra("id", templates.get(getAdapterPosition()).getId());
                    context.startActivity(intent);
                }
            });
        }
    }
}
