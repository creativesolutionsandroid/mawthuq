package com.cs.mawthuq.Listeners;

public interface ContactSelectedListener {

    void onContactSelected(int position);

}
