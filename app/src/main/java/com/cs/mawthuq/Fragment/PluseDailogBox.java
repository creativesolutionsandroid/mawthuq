package com.cs.mawthuq.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.cs.mawthuq.Activitys.AskSignatureActivity;
import com.cs.mawthuq.Activitys.DashBordActivity;
import com.cs.mawthuq.Activitys.SelectTypeActivity;
import com.cs.mawthuq.Activitys.WriteAContractActivity;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class PluseDailogBox extends BottomSheetDialogFragment {

    View rootView, layoutview;

    RelativeLayout bottomLayout;
    TextView createletter, createcontract, cancle;

    public static PluseDailogBox newInstance() {
        return new PluseDailogBox();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.bottom_dailogbox, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        createletter = (TextView) rootView.findViewById(R.id.createlatter);
        createcontract = (TextView) rootView.findViewById(R.id.createcontract);
        cancle = (TextView) rootView.findViewById(R.id.cancle);

        createcontract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                Constants.DOCUMENT_OPTION = KeyConstants.ID_CONTRACT;
                Intent intent = new Intent(getActivity(), SelectTypeActivity.class);
                startActivity(intent);
            }
        });
        createletter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                Constants.DOCUMENT_OPTION = KeyConstants.ID_LETTER;
                Intent intent = new Intent(getActivity(), SelectTypeActivity.class);
                startActivity(intent);
            }
        });
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return rootView;
    }

}
