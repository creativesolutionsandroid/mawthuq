package com.cs.mawthuq.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.mawthuq.Adapters.InboxAdapter;
import com.cs.mawthuq.Adapters.OutboxAdapter;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.R;

import java.util.ArrayList;

public class OutboxLetterFragment extends Fragment {

    private View rootView;
    private ArrayList<InboxResponse.Data> templates = new ArrayList<>();
    OutboxAdapter mAdapter;
    RecyclerView inboxListView;
    TextView noData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_inbox, container, false);

        templates = (ArrayList<InboxResponse.Data>) getArguments().getSerializable("inbox");

        inboxListView = (RecyclerView) rootView.findViewById(R.id.inbox_list);
        noData = (TextView) rootView.findViewById(R.id.no_documents_found);

        mAdapter = new OutboxAdapter(getContext(), templates);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        inboxListView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        inboxListView.setAdapter(mAdapter);

        if (templates.size() == 0) {
            noData.setVisibility(View.VISIBLE);
        }

        return rootView;
    }
}
