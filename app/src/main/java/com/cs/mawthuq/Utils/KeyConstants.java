package com.cs.mawthuq.Utils;

public class KeyConstants {

    public static String KEY_USERID = "USERID";
    public static String KEY_USERNAME = "USERNAME";
    public static String KEY_PHONE = "PHONENUMBER";
    public static String KEY_PROFILE_PIC = "PROFILEPIC";
    public static String KEY_SIGNATURE = "SIGNATURE";
    public static String KEY_PASSWORD = "PASSWORD";
    public static String KEY_EMAIL = "EMAIL";
    public static String KEY_ROLETYPE = "ROLETYPE";

    public static int ID_LETTER = 1;
    public static int ID_CONTRACT = 2;

    public static int TYPE_TEMPLATE = 1;
    public static int TYPE_SCAN = 2;
    public static int TYPE_WRITE = 3;

}
