package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HistoryResponse {

    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Data")
    private List<Data> Data;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public List<Data> getData() {
        return Data;
    }

    public void setData(List<Data> Data) {
        this.Data = Data;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("SignatureType")
        private String SignatureType;
        @Expose
        @SerializedName("ReferenceNo")
        private String ReferenceNo;
        @Expose
        @SerializedName("SendorJSON")
        private String SendorJSON;
        @Expose
        @SerializedName("DocSentDate")
        private String DocSentDate;
        @Expose
        @SerializedName("DocSignedDate")
        private String DocSignedDate;
        @Expose
        @SerializedName("SenderId")
        private int SenderId;
        @Expose
        @SerializedName("IsSubmitted")
        private boolean IsSubmitted;
        @Expose
        @SerializedName("ReceiverName")
        private String ReceiverName;
        @Expose
        @SerializedName("SenderName")
        private String SenderName;
        @Expose
        @SerializedName("ReceiverId")
        private int ReceiverId;
        @Expose
        @SerializedName("DocumentStatus")
        private int DocumentStatus;
        @Expose
        @SerializedName("TemplateTypeNameEn")
        private String TemplateTypeNameEn;
        @Expose
        @SerializedName("TemplateType")
        private int TemplateType;
        @Expose
        @SerializedName("TemplateNameAr")
        private String TemplateNameAr;
        @Expose
        @SerializedName("TemplateNameEn")
        private String TemplateNameEn;
        @Expose
        @SerializedName("ReceiverImage")
        private String ReceiverImage;
        @Expose
        @SerializedName("SenderImage")
        private String SenderImage;
        @Expose
        @SerializedName("TemplateId")
        private int TemplateId;
        @Expose
        @SerializedName("TemplateOption")
        private int TemplateOption;

        public String getSenderName() {
            return SenderName;
        }

        public void setSenderName(String senderName) {
            SenderName = senderName;
        }

        public String getReceiverImage() {
            return ReceiverImage;
        }

        public void setReceiverImage(String receiverImage) {
            ReceiverImage = receiverImage;
        }

        public String getSenderImage() {
            return SenderImage;
        }

        public void setSenderImage(String senderImage) {
            SenderImage = senderImage;
        }

        public int getTemplateOption() {
            return TemplateOption;
        }

        public void setTemplateOption(int templateOption) {
            TemplateOption = templateOption;
        }

        public String getSignatureType() {
            return SignatureType;
        }

        public void setSignatureType(String SignatureType) {
            this.SignatureType = SignatureType;
        }

        public String getReferenceNo() {
            return ReferenceNo;
        }

        public void setReferenceNo(String ReferenceNo) {
            this.ReferenceNo = ReferenceNo;
        }

        public String getSendorJSON() {
            return SendorJSON;
        }

        public void setSendorJSON(String SendorJSON) {
            this.SendorJSON = SendorJSON;
        }

        public String getDocSentDate() {
            return DocSentDate;
        }

        public void setDocSentDate(String DocSentDate) {
            this.DocSentDate = DocSentDate;
        }

        public String getDocSignedDate() {
            return DocSignedDate;
        }

        public void setDocSignedDate(String DocSignedDate) {
            this.DocSignedDate = DocSignedDate;
        }

        public int getSenderId() {
            return SenderId;
        }

        public void setSenderId(int SenderId) {
            this.SenderId = SenderId;
        }

        public boolean getIsSubmitted() {
            return IsSubmitted;
        }

        public void setIsSubmitted(boolean IsSubmitted) {
            this.IsSubmitted = IsSubmitted;
        }

        public String getReceiverName() {
            return ReceiverName;
        }

        public void setReceiverName(String ReceiverName) {
            this.ReceiverName = ReceiverName;
        }

        public int getReceiverId() {
            return ReceiverId;
        }

        public void setReceiverId(int ReceiverId) {
            this.ReceiverId = ReceiverId;
        }

        public int getDocumentStatus() {
            return DocumentStatus;
        }

        public void setDocumentStatus(int DocumentStatus) {
            this.DocumentStatus = DocumentStatus;
        }

        public String getTemplateTypeNameEn() {
            return TemplateTypeNameEn;
        }

        public void setTemplateTypeNameEn(String TemplateTypeNameEn) {
            this.TemplateTypeNameEn = TemplateTypeNameEn;
        }

        public int getTemplateType() {
            return TemplateType;
        }

        public void setTemplateType(int TemplateType) {
            this.TemplateType = TemplateType;
        }

        public String getTemplateNameAr() {
            return TemplateNameAr;
        }

        public void setTemplateNameAr(String TemplateNameAr) {
            this.TemplateNameAr = TemplateNameAr;
        }

        public String getTemplateNameEn() {
            return TemplateNameEn;
        }

        public void setTemplateNameEn(String TemplateNameEn) {
            this.TemplateNameEn = TemplateNameEn;
        }

        public int getTemplateId() {
            return TemplateId;
        }

        public void setTemplateId(int TemplateId) {
            this.TemplateId = TemplateId;
        }
    }
}
