package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ResetpasswordResponce {

    @Expose
    @SerializedName("Data")
    private List<DataEntity> Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public List<DataEntity> getData() {
        return Data;
    }

    public void setData(List<DataEntity> Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class DataEntity {
        @Expose
        @SerializedName("MessageAr")
        private String MessageAr;
        @Expose
        @SerializedName("MessageEn")
        private String MessageEn;
        @Expose
        @SerializedName("StatusCode")
        private String StatusCode;

        public String getMessageAr() {
            return MessageAr;
        }

        public void setMessageAr(String MessageAr) {
            this.MessageAr = MessageAr;
        }

        public String getMessageEn() {
            return MessageEn;
        }

        public void setMessageEn(String MessageEn) {
            this.MessageEn = MessageEn;
        }

        public String getStatusCode() {
            return StatusCode;
        }

        public void setStatusCode(String StatusCode) {
            this.StatusCode = StatusCode;
        }
    }
}


