package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class Contacts implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("UserList")
        private ArrayList<UserList> UserList;

        public ArrayList<UserList> getUserList() {
            return UserList;
        }

        public void setUserList(ArrayList<UserList> UserList) {
            this.UserList = UserList;
        }
    }

    public static class UserList implements Serializable{
        @Expose
        @SerializedName("TotalRowCount")
        private int TotalRowCount;
        @Expose
        @SerializedName("SignatureType")
        private String SignatureType;
        @Expose
        @SerializedName("IsBlock")
        private boolean IsBlock;
        @Expose
        @SerializedName("DeviceType")
        private String DeviceType;
        @Expose
        @SerializedName("Language")
        private String Language;
        @Expose
        @SerializedName("DeviceVersion")
        private String DeviceVersion;
        @Expose
        @SerializedName("DeviceToken")
        private String DeviceToken;
        @Expose
        @SerializedName("ImageName")
        private String ImageName;
        @Expose
        @SerializedName("PhoneNumber")
        private String PhoneNumber;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("RoleType")
        private String RoleType;
        @Expose
        @SerializedName("RoleId")
        private int RoleId;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("UserId")
        private int UserId;

        public int getTotalRowCount() {
            return TotalRowCount;
        }

        public void setTotalRowCount(int TotalRowCount) {
            this.TotalRowCount = TotalRowCount;
        }

        public String getSignatureType() {
            return SignatureType;
        }

        public void setSignatureType(String SignatureType) {
            this.SignatureType = SignatureType;
        }

        public boolean getIsBlock() {
            return IsBlock;
        }

        public void setIsBlock(boolean IsBlock) {
            this.IsBlock = IsBlock;
        }

        public String getDeviceType() {
            return DeviceType;
        }

        public void setDeviceType(String DeviceType) {
            this.DeviceType = DeviceType;
        }

        public String getLanguage() {
            return Language;
        }

        public void setLanguage(String Language) {
            this.Language = Language;
        }

        public String getDeviceVersion() {
            return DeviceVersion;
        }

        public void setDeviceVersion(String DeviceVersion) {
            this.DeviceVersion = DeviceVersion;
        }

        public String getDeviceToken() {
            return DeviceToken;
        }

        public void setDeviceToken(String DeviceToken) {
            this.DeviceToken = DeviceToken;
        }

        public String getImageName() {
            return ImageName;
        }

        public void setImageName(String ImageName) {
            this.ImageName = ImageName;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String PhoneNumber) {
            this.PhoneNumber = PhoneNumber;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getRoleType() {
            return RoleType;
        }

        public void setRoleType(String RoleType) {
            this.RoleType = RoleType;
        }

        public int getRoleId() {
            return RoleId;
        }

        public void setRoleId(int RoleId) {
            this.RoleId = RoleId;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }
    }
}
