package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InboxResponse implements Serializable {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("SubmittedDate")
        private String SubmittedDate;
        @Expose
        @SerializedName("IsSubmitted")
        private boolean IsSubmitted;
        @Expose
        @SerializedName("ReceiverNotes")
        private String ReceiverNotes;
        @Expose
        @SerializedName("ReceiverJSON")
        private String ReceiverJSON;
        @Expose
        @SerializedName("ReceiverImageName")
        private String ReceiverImageName;
        @Expose
        @SerializedName("ReceiverMobile")
        private String ReceiverMobile;
        @Expose
        @SerializedName("ReceiverEmail")
        private String ReceiverEmail;
        @Expose
        @SerializedName("ReceiverFullName")
        private String ReceiverFullName;
        @Expose
        @SerializedName("ReceiverId")
        private int ReceiverId;
        @Expose
        @SerializedName("SentDate")
        private String SentDate;
        @Expose
        @SerializedName("DocumentStatus")
        private int DocumentStatus;
        @Expose
        @SerializedName("TemplateId")
        private int TemplateId;
        @Expose
        @SerializedName("TemplateOption")
        private int TemplateOption;
        @Expose
        @SerializedName("TemplateType")
        private int TemplateType;
        @Expose
        @SerializedName("SendorJSON")
        private String SendorJSON;
        @Expose
        @SerializedName("ReferenceNo")
        private String ReferenceNo;
        @Expose
        @SerializedName("TemplateNameEn")
        private String TemplateNameEn;
        @Expose
        @SerializedName("SenderImageName")
        private String SenderImageName;
        @Expose
        @SerializedName("SenderMobile")
        private String SenderMobile;
        @Expose
        @SerializedName("SenderEmal")
        private String SenderEmal;
        @Expose
        @SerializedName("SenderFullName")
        private String SenderFullName;
        @Expose
        @SerializedName("SenderId")
        private int SenderId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getTemplateNameEn() {
            return TemplateNameEn;
        }

        public void setTemplateNameEn(String templateNameEn) {
            TemplateNameEn = templateNameEn;
        }

        public String getSubmittedDate() {
            return SubmittedDate;
        }

        public void setSubmittedDate(String SubmittedDate) {
            this.SubmittedDate = SubmittedDate;
        }

        public boolean getIsSubmitted() {
            return IsSubmitted;
        }

        public void setIsSubmitted(boolean IsSubmitted) {
            this.IsSubmitted = IsSubmitted;
        }

        public String getReceiverNotes() {
            return ReceiverNotes;
        }

        public void setReceiverNotes(String ReceiverNotes) {
            this.ReceiverNotes = ReceiverNotes;
        }

        public String getReceiverJSON() {
            return ReceiverJSON;
        }

        public void setReceiverJSON(String ReceiverJSON) {
            this.ReceiverJSON = ReceiverJSON;
        }

        public String getReceiverImageName() {
            return ReceiverImageName;
        }

        public void setReceiverImageName(String ReceiverImageName) {
            this.ReceiverImageName = ReceiverImageName;
        }

        public String getReceiverMobile() {
            return ReceiverMobile;
        }

        public void setReceiverMobile(String ReceiverMobile) {
            this.ReceiverMobile = ReceiverMobile;
        }

        public String getReceiverEmail() {
            return ReceiverEmail;
        }

        public void setReceiverEmail(String ReceiverEmail) {
            this.ReceiverEmail = ReceiverEmail;
        }

        public String getReceiverFullName() {
            return ReceiverFullName;
        }

        public void setReceiverFullName(String ReceiverFullName) {
            this.ReceiverFullName = ReceiverFullName;
        }

        public int getReceiverId() {
            return ReceiverId;
        }

        public void setReceiverId(int ReceiverId) {
            this.ReceiverId = ReceiverId;
        }

        public String getSentDate() {
            return SentDate;
        }

        public void setSentDate(String SentDate) {
            this.SentDate = SentDate;
        }

        public int getDocumentStatus() {
            return DocumentStatus;
        }

        public void setDocumentStatus(int DocumentStatus) {
            this.DocumentStatus = DocumentStatus;
        }

        public int getTemplateId() {
            return TemplateId;
        }

        public void setTemplateId(int TemplateId) {
            this.TemplateId = TemplateId;
        }

        public int getTemplateOption() {
            return TemplateOption;
        }

        public void setTemplateOption(int TemplateOption) {
            this.TemplateOption = TemplateOption;
        }

        public int getTemplateType() {
            return TemplateType;
        }

        public void setTemplateType(int TemplateType) {
            this.TemplateType = TemplateType;
        }

        public String getSendorJSON() {
            return SendorJSON;
        }

        public void setSendorJSON(String SendorJSON) {
            this.SendorJSON = SendorJSON;
        }

        public String getReferenceNo() {
            return ReferenceNo;
        }

        public void setReferenceNo(String ReferenceNo) {
            this.ReferenceNo = ReferenceNo;
        }

        public String getSenderImageName() {
            return SenderImageName;
        }

        public void setSenderImageName(String SenderImageName) {
            this.SenderImageName = SenderImageName;
        }

        public String getSenderMobile() {
            return SenderMobile;
        }

        public void setSenderMobile(String SenderMobile) {
            this.SenderMobile = SenderMobile;
        }

        public String getSenderEmal() {
            return SenderEmal;
        }

        public void setSenderEmal(String SenderEmal) {
            this.SenderEmal = SenderEmal;
        }

        public String getSenderFullName() {
            return SenderFullName;
        }

        public void setSenderFullName(String SenderFullName) {
            this.SenderFullName = SenderFullName;
        }

        public int getSenderId() {
            return SenderId;
        }

        public void setSenderId(int SenderId) {
            this.SenderId = SenderId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
