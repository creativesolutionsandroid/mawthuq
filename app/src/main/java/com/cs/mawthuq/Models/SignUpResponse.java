package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignUpResponse {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("RoleId")
        private int RoleId;
        @Expose
        @SerializedName("RoleType")
        private String RoleType;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("PhoneNumber")
        private String PhoneNumber;
        @Expose
        @SerializedName("ImageName")
        private String ImageName;
        @Expose
        @SerializedName("SignatureImage")
        private String SignatureImage;

        public String getSignatureImage() {
            return SignatureImage;
        }

        public void setSignatureImage(String signatureImage) {
            SignatureImage = signatureImage;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            PhoneNumber = phoneNumber;
        }

        public String getImageName() {
            return ImageName;
        }

        public void setImageName(String imageName) {
            ImageName = imageName;
        }

        public int getRoleId() {
            return RoleId;
        }

        public void setRoleId(int RoleId) {
            this.RoleId = RoleId;
        }

        public String getRoleType() {
            return RoleType;
        }

        public void setRoleType(String RoleType) {
            this.RoleType = RoleType;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }
    }
}
