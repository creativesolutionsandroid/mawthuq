package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TemplateData {

    @Expose
    @SerializedName("Template")
    private Template Template;
    @Expose
    @SerializedName("Controls")
    private Controls Controls;

    public TemplateData.Template getTemplate() {
        return Template;
    }

    public void setTemplate(TemplateData.Template template) {
        Template = template;
    }

    public TemplateData.Controls getControls() {
        return Controls;
    }

    public void setControls(TemplateData.Controls controls) {
        Controls = controls;
    }

    public static class Template{

        @Expose
        @SerializedName("OwnerId")
        private String OwnerId;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("TemplateId")
        private String TemplateId;
        @Expose
        @SerializedName("Content")
        private List<Content> Content;

        public List<TemplateData.Content> getContent() {
            return Content;
        }

        public void setContent(List<TemplateData.Content> content) {
            Content = content;
        }

        public String getOwnerId() {
            return OwnerId;
        }

        public void setOwnerId(String ownerId) {
            OwnerId = ownerId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getTemplateId() {
            return TemplateId;
        }

        public void setTemplateId(String templateId) {
            TemplateId = templateId;
        }
    }

    public static class Content {
        @Expose
        @SerializedName("time")
        private String time;
        @Expose
        @SerializedName("blocks")
        private List<Blocks> blocks;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public List<Blocks> getBlocks() {
            return blocks;
        }

        public void setBlocks(List<Blocks> blocks) {
            this.blocks = blocks;
        }
    }

    public static class Blocks {
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("data")
        private Data data;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    }

    public static class Data {
        @Expose
        @SerializedName("text")
        private String text;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    public static class Controls {
        @Expose
        @SerializedName("Controls")
        private List<ControlsArray> Controls;

        public List<ControlsArray> getControls() {
            return Controls;
        }

        public void setControls(List<ControlsArray> Controls) {
            this.Controls = Controls;
        }
    }

    public static class ControlsArray {
        @Expose
        @SerializedName("Configuration")
        private Configuration Configuration;
        @Expose
        @SerializedName("ShoartCode")
        private String ShoartCode;
        @Expose
        @SerializedName("ControlName")
        private String ControlName;

        public Configuration getConfiguration() {
            return Configuration;
        }

        public void setConfiguration(Configuration Configuration) {
            this.Configuration = Configuration;
        }

        public String getShoartCode() {
            return ShoartCode;
        }

        public void setShoartCode(String ShoartCode) {
            this.ShoartCode = ShoartCode;
        }

        public String getControlName() {
            return ControlName;
        }

        public void setControlName(String ControlName) {
            this.ControlName = ControlName;
        }
    }

    public static class Configuration {

        @Expose
        @SerializedName("Contentformat")
        private String Contentformat;
        @Expose
        @SerializedName("Options")
        private String Options;
        @Expose
        @SerializedName("Maxlength")
        private int Maxlength;
        @Expose
        @SerializedName("MinLength")
        private int MinLength;
        @Expose
        @SerializedName("MaxNumber")
        private int MaxNumber;
        @Expose
        @SerializedName("MinNumber")
        private int MinNumber;
        @Expose
        @SerializedName("RequiredBy")
        private String RequiredBy;
        @Expose
        @SerializedName("Placehoder")
        private String Placehoder;
        @Expose
        @SerializedName("Controltype")
        private String Controltype;

        public String getContentformat() {
            return Contentformat;
        }

        public void setContentformat(String Contentformat) {
            this.Contentformat = Contentformat;
        }

        public String getOptions() {
            return Options;
        }

        public void setOptions(String Options) {
            this.Options = Options;
        }

        public int getMaxlength() {
            return Maxlength;
        }

        public void setMaxlength(int Maxlength) {
            this.Maxlength = Maxlength;
        }

        public int getMinLength() {
            return MinLength;
        }

        public void setMinLength(int MinLength) {
            this.MinLength = MinLength;
        }

        public int getMaxNumber() {
            return MaxNumber;
        }

        public void setMaxNumber(int MaxNumber) {
            this.MaxNumber = MaxNumber;
        }

        public int getMinNumber() {
            return MinNumber;
        }

        public void setMinNumber(int MinNumber) {
            this.MinNumber = MinNumber;
        }

        public String getRequiredBy() {
            return RequiredBy;
        }

        public void setRequiredBy(String RequiredBy) {
            this.RequiredBy = RequiredBy;
        }

        public String getPlacehoder() {
            return Placehoder;
        }

        public void setPlacehoder(String Placehoder) {
            this.Placehoder = Placehoder;
        }

        public String getControltype() {
            return Controltype;
        }

        public void setControltype(String Controltype) {
            this.Controltype = Controltype;
        }
    }
}
