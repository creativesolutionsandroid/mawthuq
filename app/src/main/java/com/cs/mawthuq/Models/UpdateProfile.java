package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateProfile {

    @Expose
    @SerializedName("Data")
    private List<Data> Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public List<Data> getData() {
        return Data;
    }

    public void setData(List<Data> Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("Ar")
        private List<Ar> Ar;
        @Expose
        @SerializedName("RoleId")
        private int RoleId;
        @Expose
        @SerializedName("ImageName")
        private String ImageName;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("PhoneNumber")
        private String PhoneNumber;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("UserId")
        private int UserId;

        public List<Ar> getAr() {
            return Ar;
        }

        public void setAr(List<Ar> Ar) {
            this.Ar = Ar;
        }

        public int getRoleId() {
            return RoleId;
        }

        public void setRoleId(int RoleId) {
            this.RoleId = RoleId;
        }

        public String getImageName() {
            return ImageName;
        }

        public void setImageName(String ImageName) {
            this.ImageName = ImageName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String PhoneNumber) {
            this.PhoneNumber = PhoneNumber;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }
    }

    public static class Ar {
        @Expose
        @SerializedName("DeviceToken")
        private String DeviceToken;
        @Expose
        @SerializedName("DeviceVersion")
        private String DeviceVersion;
        @Expose
        @SerializedName("DeviceType")
        private String DeviceType;
        @Expose
        @SerializedName("RoleType")
        private String RoleType;

        public String getDeviceToken() {
            return DeviceToken;
        }

        public void setDeviceToken(String DeviceToken) {
            this.DeviceToken = DeviceToken;
        }

        public String getDeviceVersion() {
            return DeviceVersion;
        }

        public void setDeviceVersion(String DeviceVersion) {
            this.DeviceVersion = DeviceVersion;
        }

        public String getDeviceType() {
            return DeviceType;
        }

        public void setDeviceType(String DeviceType) {
            this.DeviceType = DeviceType;
        }

        public String getRoleType() {
            return RoleType;
        }

        public void setRoleType(String RoleType) {
            this.RoleType = RoleType;
        }
    }
}
