package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DashboardResponse implements Serializable{

    @Expose
    @SerializedName("DraftDocument")
    private List<DraftDocument> DraftDocument;
    @Expose
    @SerializedName("UnreadMessages")
    private List<UnreadMessages> UnreadMessages;
    @Expose
    @SerializedName("NotificationCount")
    private int NotificationCount;
    @Expose
    @SerializedName("WaitingForOtherCount")
    private String WaitingForOtherCount;
    @Expose
    @SerializedName("WatingForYouCount")
    private String WatingForYouCount;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public List<DraftDocument> getDraftDocument() {
        return DraftDocument;
    }

    public void setDraftDocument(List<DraftDocument> DraftDocument) {
        this.DraftDocument = DraftDocument;
    }

    public List<UnreadMessages> getUnreadMessages() {
        return UnreadMessages;
    }

    public void setUnreadMessages(List<UnreadMessages> UnreadMessages) {
        this.UnreadMessages = UnreadMessages;
    }

    public int getNotificationCount() {
        return NotificationCount;
    }

    public void setNotificationCount(int NotificationCount) {
        this.NotificationCount = NotificationCount;
    }

    public String getWaitingForOtherCount() {
        return WaitingForOtherCount;
    }

    public void setWaitingForOtherCount(String WaitingForOtherCount) {
        this.WaitingForOtherCount = WaitingForOtherCount;
    }

    public String getWatingForYouCount() {
        return WatingForYouCount;
    }

    public void setWatingForYouCount(String WatingForYouCount) {
        this.WatingForYouCount = WatingForYouCount;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class DraftDocument implements Serializable {
        @Expose
        @SerializedName("Draftdate")
        private String Draftdate;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("Mobile")
        private String Mobile;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("ReceiverID")
        private int ReceiverID;
        @Expose
        @SerializedName("SenderNotes")
        private String SenderNotes;
        @Expose
        @SerializedName("SenderJSON")
        private String SenderJSON;
        @Expose
        @SerializedName("TemplateOption")
        private int TemplateOption;
        @Expose
        @SerializedName("TemplateType")
        private int TemplateType;
        @Expose
        @SerializedName("TemplateNameAr")
        private String TemplateNameAr;
        @Expose
        @SerializedName("TemplateNameEn")
        private String TemplateNameEn;
        @Expose
        @SerializedName("TemplateId")
        private int TemplateId;
        @Expose
        @SerializedName("SenderId")
        private int SenderId;
        @Expose
        @SerializedName("DraftId")
        private int DraftId;

        public String getDraftdate() {
            return Draftdate;
        }

        public void setDraftdate(String Draftdate) {
            this.Draftdate = Draftdate;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public int getReceiverID() {
            return ReceiverID;
        }

        public void setReceiverID(int ReceiverID) {
            this.ReceiverID = ReceiverID;
        }

        public String getSenderNotes() {
            return SenderNotes;
        }

        public void setSenderNotes(String SenderNotes) {
            this.SenderNotes = SenderNotes;
        }

        public String getSenderJSON() {
            return SenderJSON;
        }

        public void setSenderJSON(String SenderJSON) {
            this.SenderJSON = SenderJSON;
        }

        public int getTemplateOption() {
            return TemplateOption;
        }

        public void setTemplateOption(int TemplateOption) {
            this.TemplateOption = TemplateOption;
        }

        public int getTemplateType() {
            return TemplateType;
        }

        public void setTemplateType(int TemplateType) {
            this.TemplateType = TemplateType;
        }

        public String getTemplateNameAr() {
            return TemplateNameAr;
        }

        public void setTemplateNameAr(String TemplateNameAr) {
            this.TemplateNameAr = TemplateNameAr;
        }

        public String getTemplateNameEn() {
            return TemplateNameEn;
        }

        public void setTemplateNameEn(String TemplateNameEn) {
            this.TemplateNameEn = TemplateNameEn;
        }

        public int getTemplateId() {
            return TemplateId;
        }

        public void setTemplateId(int TemplateId) {
            this.TemplateId = TemplateId;
        }

        public int getSenderId() {
            return SenderId;
        }

        public void setSenderId(int SenderId) {
            this.SenderId = SenderId;
        }

        public int getDraftId() {
            return DraftId;
        }

        public void setDraftId(int DraftId) {
            this.DraftId = DraftId;
        }
    }

    public static class UnreadMessages implements Serializable{
        @Expose
        @SerializedName("UnreadCount")
        private int UnreadCount;
        @Expose
        @SerializedName("DocumentId")
        private int DocumentId;
        @Expose
        @SerializedName("ProfileImage")
        private String ProfileImage;
        @Expose
        @SerializedName("FullName")
        private String SenderName;
        @Expose
        @SerializedName("UserId")
        private int UserId;

        public int getUnreadCount() {
            return UnreadCount;
        }

        public void setUnreadCount(int UnreadCount) {
            this.UnreadCount = UnreadCount;
        }

        public int getDocumentId() {
            return DocumentId;
        }

        public void setDocumentId(int DocumentId) {
            this.DocumentId = DocumentId;
        }

        public String getProfileImage() {
            return ProfileImage;
        }

        public void setProfileImage(String ProfileImage) {
            this.ProfileImage = ProfileImage;
        }

        public String getSenderName() {
            return SenderName;
        }

        public void setSenderName(String SenderName) {
            this.SenderName = SenderName;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }
    }
}
