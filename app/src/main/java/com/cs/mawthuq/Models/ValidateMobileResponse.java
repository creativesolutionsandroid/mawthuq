package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidateMobileResponse {

    @Expose
    @SerializedName("Data")
    public Data Data;
    @Expose
    @SerializedName("MessageAr")
    public String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    public String MessageEn;
    @Expose
    @SerializedName("Status")
    public boolean Status;

    public static class Data {
        @Expose
        @SerializedName("OtpCode")
        public String OtpCode;

        public String getOtpCode() {
            return OtpCode;
        }

        public void setOtpCode(String otpCode) {
            OtpCode = otpCode;
        }
    }

    public ValidateMobileResponse.Data getData() {
        return Data;
    }

    public void setData(ValidateMobileResponse.Data data) {
        Data = data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String messageEn) {
        MessageEn = messageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }
}
