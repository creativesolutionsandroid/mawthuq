package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Drafts implements Serializable {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("Mobile")
        private String Mobile;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("ReceiverID")
        private int ReceiverID;
        @Expose
        @SerializedName("SenderNotes")
        private String SenderNotes;
        @Expose
        @SerializedName("SenderJSON")
        private String SenderJSON;
        @Expose
        @SerializedName("TemplateOption")
        private int TemplateOption;
        @Expose
        @SerializedName("TemplateType")
        private int TemplateType;
        @Expose
        @SerializedName("TemplateNameAr")
        private String TemplateNameAr;
        @Expose
        @SerializedName("TemplateNameEn")
        private String TemplateNameEn;
        @Expose
        @SerializedName("TemplateId")
        private int TemplateId;
        @Expose
        @SerializedName("SenderId")
        private int SenderId;
        @Expose
        @SerializedName("DraftId")
        private int DraftId;
        @Expose
        @SerializedName("Draftdate")
        private String Draftdate;
        @Expose
        @SerializedName("ReceiverImage")
        private String ReceiverImage;

        public String getReceiverImage() {
            return ReceiverImage;
        }

        public void setReceiverImage(String receiverImage) {
            ReceiverImage = receiverImage;
        }

        public String getDraftdate() {
            return Draftdate;
        }

        public void setDraftdate(String draftdate) {
            Draftdate = draftdate;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public int getReceiverID() {
            return ReceiverID;
        }

        public void setReceiverID(int ReceiverID) {
            this.ReceiverID = ReceiverID;
        }

        public String getSenderNotes() {
            return SenderNotes;
        }

        public void setSenderNotes(String SenderNotes) {
            this.SenderNotes = SenderNotes;
        }

        public String getSenderJSON() {
            return SenderJSON;
        }

        public void setSenderJSON(String SenderJSON) {
            this.SenderJSON = SenderJSON;
        }

        public int getTemplateOption() {
            return TemplateOption;
        }

        public void setTemplateOption(int TemplateOption) {
            this.TemplateOption = TemplateOption;
        }

        public int getTemplateType() {
            return TemplateType;
        }

        public void setTemplateType(int TemplateType) {
            this.TemplateType = TemplateType;
        }

        public String getTemplateNameAr() {
            return TemplateNameAr;
        }

        public void setTemplateNameAr(String TemplateNameAr) {
            this.TemplateNameAr = TemplateNameAr;
        }

        public String getTemplateNameEn() {
            return TemplateNameEn;
        }

        public void setTemplateNameEn(String TemplateNameEn) {
            this.TemplateNameEn = TemplateNameEn;
        }

        public int getTemplateId() {
            return TemplateId;
        }

        public void setTemplateId(int TemplateId) {
            this.TemplateId = TemplateId;
        }

        public int getSenderId() {
            return SenderId;
        }

        public void setSenderId(int SenderId) {
            this.SenderId = SenderId;
        }

        public int getDraftId() {
            return DraftId;
        }

        public void setDraftId(int DraftId) {
            this.DraftId = DraftId;
        }
    }
}
