package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class TemplateList implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("Template")
        private ArrayList<Template> Template;

        public ArrayList<Template> getTemplate() {
            return Template;
        }

        public void setTemplate(ArrayList<Template> Template) {
            this.Template = Template;
        }
    }

    public static class Template implements Serializable{
        @Expose
        @SerializedName("FavouriteTemplate")
        private boolean FavouriteTemplate;
        @Expose
        @SerializedName("TemplatejsonEn")
        private String TemplatejsonEn;
        @Expose
        @SerializedName("TotalRowCount")
        private int TotalRowCount;
        @Expose
        @SerializedName("DescriptionAr")
        private String DescriptionAr;
        @Expose
        @SerializedName("DescriptionEn")
        private String DescriptionEn;
        @Expose
        @SerializedName("TemplateTypeNameEn")
        private String TemplateTypeNameEn;
        @Expose
        @SerializedName("TemplateType")
        private int TemplateType;
        @Expose
        @SerializedName("TemplateNameAr")
        private String TemplateNameAr;
        @Expose
        @SerializedName("TemplateNameEn")
        private String TemplateNameEn;
        @Expose
        @SerializedName("RowNumber")
        private int RowNumber;
        @Expose
        @SerializedName("TemplateId")
        private int TemplateId;

        public boolean getFavouriteTemplate() {
            return FavouriteTemplate;
        }

        public void setFavouriteTemplate(boolean FavouriteTemplate) {
            this.FavouriteTemplate = FavouriteTemplate;
        }

        public String getTemplatejsonEn() {
            return TemplatejsonEn;
        }

        public void setTemplatejsonEn(String TemplatejsonEn) {
            this.TemplatejsonEn = TemplatejsonEn;
        }

        public int getTotalRowCount() {
            return TotalRowCount;
        }

        public void setTotalRowCount(int TotalRowCount) {
            this.TotalRowCount = TotalRowCount;
        }

        public String getDescriptionAr() {
            return DescriptionAr;
        }

        public void setDescriptionAr(String DescriptionAr) {
            this.DescriptionAr = DescriptionAr;
        }

        public String getDescriptionEn() {
            return DescriptionEn;
        }

        public void setDescriptionEn(String DescriptionEn) {
            this.DescriptionEn = DescriptionEn;
        }

        public String getTemplateTypeNameEn() {
            return TemplateTypeNameEn;
        }

        public void setTemplateTypeNameEn(String TemplateTypeNameEn) {
            this.TemplateTypeNameEn = TemplateTypeNameEn;
        }

        public int getTemplateType() {
            return TemplateType;
        }

        public void setTemplateType(int TemplateType) {
            this.TemplateType = TemplateType;
        }

        public String getTemplateNameAr() {
            return TemplateNameAr;
        }

        public void setTemplateNameAr(String TemplateNameAr) {
            this.TemplateNameAr = TemplateNameAr;
        }

        public String getTemplateNameEn() {
            return TemplateNameEn;
        }

        public void setTemplateNameEn(String TemplateNameEn) {
            this.TemplateNameEn = TemplateNameEn;
        }

        public int getRowNumber() {
            return RowNumber;
        }

        public void setRowNumber(int RowNumber) {
            this.RowNumber = RowNumber;
        }

        public int getTemplateId() {
            return TemplateId;
        }

        public void setTemplateId(int TemplateId) {
            this.TemplateId = TemplateId;
        }
    }
}
