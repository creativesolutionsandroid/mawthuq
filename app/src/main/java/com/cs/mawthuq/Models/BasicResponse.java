package com.cs.mawthuq.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BasicResponse {

    @Expose
    @SerializedName("MessageAr")
    public String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    public String MessageEn;
    @Expose
    @SerializedName("Status")
    public boolean Status;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String messageEn) {
        MessageEn = messageEn;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }
}
