package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.mawthuq.Adapters.ChatListAdapter;
import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.ChatListResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class ChatActivity extends AppCompatActivity {

    TextView title, name;
    ImageView back_btn, send;
    View send_bg;
    EditText search, etMessage;
    RecyclerView contactsList;
    ChatListAdapter mAdapter;
    private ArrayList<ChatListResponse.Data> chats = new ArrayList<>();
    SharedPreferences userPrefs;
    boolean isFirstTime = true;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        title = (TextView) findViewById(R.id.title);
        name = (TextView) findViewById(R.id.chat1_name);
        search = (EditText) findViewById(R.id.search);
        etMessage = (EditText) findViewById(R.id.et_message);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        send = (ImageView) findViewById(R.id.send);
        send_bg = (View) findViewById(R.id.send_bg);
        contactsList = (RecyclerView) findViewById(R.id.contacts_list);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etMessage.getText().toString().trim().length() == 0) {
                    etMessage.setError("Please enter message");
                }
                else {
                    Constants.hideKeyboard(ChatActivity.this);
                    new updateChatApi().execute();
                }
            }
        });
    }

    private void disableSend() {
        send.setClickable(false);
        send.setAlpha(0.4f);
        send_bg.setAlpha(0.4f);
    }

    private void enableSend() {
        send.setClickable(true);
        send.setAlpha(1.0f);
        send_bg.setAlpha(1.0f);
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("DocumentId", getIntent().getIntExtra("DocumentId", 0));
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class getChatApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            chats.clear();
            if (isFirstTime) {
                Constants.showLoadingDialog(ChatActivity.this);
                isFirstTime = false;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ChatActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<ChatListResponse> call = apiService.getDocChat(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChatListResponse>() {
                @Override
                public void onResponse(Call<ChatListResponse> call, Response<ChatListResponse> response) {
                    if (response.isSuccessful()) {
                        ChatListResponse ChatListResponse = response.body();
                        Log.d("TAG", "onResponse: " + response.toString());
                        try {
                            chats = ChatListResponse.getData();
                            if (!ChatListResponse.getSenderName().equals(userPrefs.getString(KeyConstants.KEY_USERNAME, ""))) {
                                name.setText(ChatListResponse.getSenderName());
                            }
                            else {
                                name.setText(ChatListResponse.getReceiverName());
                            }
                            title.setText(ChatListResponse.getDocumentName());

                            mAdapter = new ChatListAdapter(ChatActivity.this, chats);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this);
                            contactsList.setLayoutManager(new GridLayoutManager(ChatActivity.this, 1));
                            contactsList.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ChatActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ChatActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ChatListResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ChatActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChatActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareSendJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("DocumentId", getIntent().getIntExtra("DocumentId", 0));
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 0));
            parentObj.put("Chattext", etMessage.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class updateChatApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSendJson();
            disableSend();
//            Constants.showLoadingDialog(ChatActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ChatActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<BasicResponse> call = apiService.saveDocChat(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    enableSend();
//                    Constants.closeLoadingDialog();
                    if (response.isSuccessful()) {
                        BasicResponse BasicResponse = response.body();
                        Log.d("TAG", "onResponse: " + response.body());
                        try {
                            if (BasicResponse.isStatus()) {
                                etMessage.setText("");
                                new getChatApi().execute();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ChatActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ChatActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    enableSend();
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ChatActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChatActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
//                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new getChatApi().execute();
    }
}