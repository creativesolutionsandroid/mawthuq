package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.cs.mawthuq.Adapters.WelcomeViewpagerAdapter;
import com.cs.mawthuq.Models.ViewpagerResponce;
import com.cs.mawthuq.R;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GetStartedScreen extends Activity {

    ViewPager textviewpager;
    Button getstart;
    ArrayList<ViewpagerResponce> textarray= new ArrayList<>();
    WelcomeViewpagerAdapter mAdapter;
    CircleIndicator defaultIndicator;
    DotsIndicator springDotsIndicator;
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getstartscreen);

        textviewpager=(ViewPager)findViewById(R.id.textviewpager);
        defaultIndicator = (CircleIndicator) findViewById(R.id.circle);
        getstart=(Button)findViewById(R.id.getstartbtn);

        defaultIndicator.createIndicators(3,0);

        textviewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                defaultIndicator.createIndicators(3, position);
//                        defaultIndicator.animatePageSelected(position);
            }

            @Override
            public void onPageSelected(int position) {
                defaultIndicator.animatePageSelected(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        getstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(GetStartedScreen.this, SignInActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        Dataassings();
    }

    private void Dataassings(){

    ViewpagerResponce viewpagerResponce=new ViewpagerResponce();
    viewpagerResponce.setTittle("Quick");
    viewpagerResponce.setDsc("Lorem ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
    textarray.add(viewpagerResponce);

    ViewpagerResponce viewpagerResponce1=new ViewpagerResponce();
    viewpagerResponce1.setTittle("Easy");
    viewpagerResponce1.setDsc("Lorem ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
    textarray.add(viewpagerResponce1);

    ViewpagerResponce viewpagerResponce2=new ViewpagerResponce();
    viewpagerResponce2.setTittle("Security");
    viewpagerResponce2.setDsc("Lorem ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
    textarray.add(viewpagerResponce2);

        mAdapter = new WelcomeViewpagerAdapter(getApplication(), textarray);
        defaultIndicator.setViewPager(textviewpager);
        textviewpager.setAdapter(mAdapter);
//        springDotsIndicator.setViewPager(textviewpager);

    }

}
