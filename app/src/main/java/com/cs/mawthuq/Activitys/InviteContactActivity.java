package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.mawthuq.Adapters.AddContactAdapter;
import com.cs.mawthuq.Adapters.ContactsListAdapter;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.Models.LocalContacts;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class InviteContactActivity extends AppCompatActivity {

    ImageView back_btn;
    TextView title;
    EditText search;
    RecyclerView contactsList;
    LinearLayout addContactLayout;
    private ArrayList<Contacts.UserList> contacts = new ArrayList<>();
    private ArrayList<LocalContacts> localContacts = new ArrayList<>();
    private ArrayList<LocalContacts> localContactsFiltered = new ArrayList<>();
    SharedPreferences userPrefs;
    AddContactAdapter mAdapter;
    public static final int RequestPermissionCode = 1;
    Cursor cursor;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact_activitty);

        contacts = (ArrayList<Contacts.UserList>) getIntent().getSerializableExtra("contacts");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        title = (TextView) findViewById(R.id.title);
        search = (EditText) findViewById(R.id.search);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        addContactLayout = (LinearLayout) findViewById(R.id.layout_addcontact);
        contactsList = (RecyclerView) findViewById(R.id.contacts_list);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addContactLayout.setVisibility(View.GONE);
        title.setText("Add Contact");

        EnableRuntimePermission();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String searchText = editable.toString();
                localContactsFiltered.clear();
                for (int i = 0; i < localContacts.size(); i++) {
                    if (localContacts.get(i).getName().toLowerCase().contains(searchText) ||
                            localContacts.get(i).getPhoneNumber().contains(searchText)) {
                        localContactsFiltered.add(localContacts.get(i));
                    }
                }

                if (localContactsFiltered.size() == 0) {
                    if (TextUtils.isDigitsOnly(searchText) && searchText.length() >= 9) {
                        LocalContacts localContacts = new LocalContacts();
                        localContacts.setPhoneNumber(searchText);
                        localContacts.setName("-");
                        localContacts.setImage("");
                        localContactsFiltered.add(localContacts);
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        search.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Constants.hideKeyboard(InviteContactActivity.this);
                    return true;
                }
                return false;
            }
        });
    }

    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(
                InviteContactActivity.this,
                Manifest.permission.READ_CONTACTS)) {

            GetContactsIntoArrayList();

        } else {
            ActivityCompat.requestPermissions(InviteContactActivity.this, new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    GetContactsIntoArrayList();
                } else {
                    Toast.makeText(InviteContactActivity.this, "Read Contacts permission rejected, unable to access CONTACTS.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void GetContactsIntoArrayList() {

        cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        Constants.showLoadingDialog(this);

        ArrayList<String> dbPhoneNumbers = new ArrayList<>();
        for (int i = 0; i < contacts.size(); i++) {
            dbPhoneNumbers.add(contacts.get(i).getPhoneNumber());
        }

        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String image = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

            String filteredPhone = phonenumber.replace("+", "");
            filteredPhone = filteredPhone.replace(" ", "");

            if (!dbPhoneNumbers.contains(filteredPhone)) {
                LocalContacts contacts = new LocalContacts();
                contacts.setName(name);
                contacts.setPhoneNumber(phonenumber);
                contacts.setImage(image);
                localContacts.add(contacts);
            }
        }
        cursor.close();

        if (localContacts.size() > 0) {
            localContactsFiltered.addAll(localContacts);
            mAdapter = new AddContactAdapter(InviteContactActivity.this, localContactsFiltered, InviteContactActivity.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InviteContactActivity.this);
            contactsList.setLayoutManager(new GridLayoutManager(InviteContactActivity.this, 1));
            contactsList.setAdapter(mAdapter);
        } else {
            Constants.showOneButtonAlertDialog("No contacts found.", getResources().getString(R.string.app_name),
                    getResources().getString(R.string.ok), InviteContactActivity.this);
        }
        Constants.closeLoadingDialog();
    }
}