package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.mawthuq.Adapters.TemplatesListAdapter;
import com.cs.mawthuq.Listeners.TemplateFavListener;
import com.cs.mawthuq.Models.TemplateList;
import com.cs.mawthuq.Models.TemplateList;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_EMAIL;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PROFILE_PIC;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERNAME;

public class TemplatesActivity extends Activity {

    TextView noTemplates;
    ImageView back_btn;
    RecyclerView templatesListView;
    TemplatesListAdapter mAdapter;
    private ArrayList<TemplateList.Template> templates = new ArrayList<>();
    private ArrayList<TemplateList.Template> templatesFiltered = new ArrayList<>();
    String TAG = "TAG";
    SharedPreferences userPrefs;
    private TemplateFavListener favListener;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.templates_activity);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        noTemplates = (TextView) findViewById(R.id.no_templates_text);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        templatesListView = (RecyclerView) findViewById(R.id.template_list);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        new getTemplateListApi().execute();

        favListener = new TemplateFavListener() {
            @Override
            public void onFavUpdated() {
                new getTemplateListApi().execute();
            }
        };
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("TemplateId", "0");
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 0));
            parentObj.put("PageSize", 100);
            parentObj.put("PageNumber", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class getTemplateListApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            Constants.showLoadingDialog(TemplatesActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(TemplatesActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<TemplateList> call = apiService.getTemplateList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<TemplateList>() {
                @Override
                public void onResponse(Call<TemplateList> call, Response<TemplateList> response) {
                    if (response.isSuccessful()) {
                        TemplateList TemplateList = response.body();
                        Log.d(TAG, "onResponse: " + response.body());
                        try {
                            if (TemplateList.getStatus()) {
                                templates = TemplateList.getData().getTemplate();

                                if (Constants.DOCUMENT_OPTION == 0 ) {
                                    templatesFiltered.addAll(templates);
                                }
                                else {
                                    for (int i = 0; i < templates.size(); i++) {
                                        if (templates.get(i).getTemplateType() == Constants.DOCUMENT_OPTION) {
                                            templatesFiltered.add(templates.get(i));
                                        }
                                    }
                                }

                                if (templatesFiltered.size() == 0) {
                                    noTemplates.setVisibility(View.VISIBLE);
                                }

                                mAdapter = new TemplatesListAdapter(TemplatesActivity.this, templatesFiltered,
                                        TemplatesActivity.this, favListener);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(TemplatesActivity.this);
                                templatesListView.setLayoutManager(new GridLayoutManager(TemplatesActivity.this, 1));
                                templatesListView.setAdapter(mAdapter);
                            } else {
                                String failureResponse = TemplateList.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), TemplatesActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(TemplatesActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(TemplatesActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<TemplateList> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(TemplatesActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(TemplatesActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
