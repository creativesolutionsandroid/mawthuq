package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.mawthuq.Models.ResetpasswordResponce;
import com.cs.mawthuq.Models.ResetpasswordResponce;
import com.cs.mawthuq.Models.SignUpResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_EMAIL;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PASSWORD;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_ROLETYPE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERNAME;

public class ResetPasswordActivity extends AppCompatActivity {

    ImageView back_btn;
    Button done ;
    EditText newpassword , confirampassword ;
    String strnewpassword,strconfpassword,strphone,strotp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resetpassword_activity);

        back_btn = (ImageView) findViewById(R.id.backbtn);
        done = (Button) findViewById(R.id.verfiybtn);
        newpassword = (EditText) findViewById(R.id.new_password);
        confirampassword = (EditText) findViewById(R.id.confiram_password);

        strotp =getIntent().getStringExtra("otp");
        strphone =getIntent().getStringExtra("mobile");

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (validations()){
                  new  ResetUpApi().execute();

               }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private boolean validations(){
        strnewpassword=newpassword.getText().toString();
        strconfpassword=confirampassword.getText().toString();

        if (strnewpassword.length()==0){
            newpassword.setError(getString(R.string.error_password));
            return false;
        }

        if (strconfpassword.length()<5){
            confirampassword.setError(getString(R.string.invalid_password));
            return false;
        }

        return true;
    }

    private class ResetUpApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
            Constants.showLoadingDialog(ResetPasswordActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ResetPasswordActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<ResetpasswordResponce> call = apiService.userResetpassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ResetpasswordResponce>() {
                @Override
                public void onResponse(Call<ResetpasswordResponce> call, Response<ResetpasswordResponce> response) {
                    if (response.isSuccessful()) {
                        ResetpasswordResponce ResetpasswordResponce = response.body();
                        Log.d("TAG", "onResponse: "+response.body());
                        try {
                            if (ResetpasswordResponce.getStatus()) {

//                                Toast.makeText(ResetPasswordActivity.this, ResetpasswordResponce.getMessageEn(), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(ResetPasswordActivity.this, SignInActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                com.cs.mawthuq.Utils.Constants.showOneButtonAlertDialogWithIntent(ResetpasswordResponce.getMessageEn(), getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ResetPasswordActivity.this, intent);
                            } else {
                                String failureResponse = ResetpasswordResponce.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ResetPasswordActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ResetPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ResetPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }



                @Override
                public void onFailure(Call<ResetpasswordResponce> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ResetPasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ResetPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareSignUpJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("PhoneNumber","966"+strphone );
            parentObj.put("Password",strnewpassword);
            parentObj.put("OTP",strotp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }


}