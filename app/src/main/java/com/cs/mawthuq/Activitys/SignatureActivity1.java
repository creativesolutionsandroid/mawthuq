package com.cs.mawthuq.Activitys;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.cs.mawthuq.Models.UpdateProfile;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.github.gcacace.signaturepad.views.SignaturePad;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_EMAIL;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_SIGNATURE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERNAME;

public class SignatureActivity1 extends AppCompatActivity {

    ImageView back_btn, signature;
    RelativeLayout signaturelayout;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    TextView update, mClear, mGetSign;
    ImageView mCancel;
    File file;
//    LinearLayout mContent;
    SignaturePad signaturePad;
    View view;
    Bitmap bitmap;
    String image;
    public AlertDialog customDialog;

    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        back_btn = (ImageView) findViewById(R.id.bak_btn);
        signature = (ImageView) findViewById(R.id.signature_image);
        signaturelayout = (RelativeLayout) findViewById(R.id.signaturelayout);
        update = (TextView) findViewById(R.id.update);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (!userPrefs.getString(KEY_SIGNATURE, "").equalsIgnoreCase("")) {
            try {
                byte[] decodedString = Base64.decode(userPrefs.getString(KEY_SIGNATURE, ""), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                signature.setImageBitmap(decodedByte);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            update.setText("Add");
        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (canAccessStorage()) {
                        showSignatureAlertDialog();
                    }
                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    }
                }
                else {
                    showSignatureAlertDialog();
                }
            }
        });
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, perm));
    }

    public void showSignatureAlertDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignatureActivity1.this);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.landscape_asksignature_activity;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        mClear = (TextView) dialogView.findViewById(R.id.clear);
        mGetSign = (TextView) dialogView.findViewById(R.id.save);
        mCancel = (ImageView) dialogView.findViewById(R.id.bak_btn1);
        signaturePad = (SignaturePad) dialogView.findViewById(R.id.signature_pad);

        mGetSign.setVisibility(View.INVISIBLE);
        mClear.setVisibility(View.INVISIBLE);

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                mGetSign.setVisibility(View.VISIBLE);
                mClear.setVisibility(View.VISIBLE);
            }

            @Override
            public void onClear() {
                mGetSign.setVisibility(View.INVISIBLE);
                mClear.setVisibility(View.INVISIBLE);
            }
        });
        mGetSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == mGetSign) {
                    bitmap = signaturePad.getTransparentSignatureBitmap();
                    signature.setImageBitmap(bitmap);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] image1Bytes = stream.toByteArray();
                    image = Base64.encodeToString(image1Bytes, Base64.DEFAULT);

                    new updateProfileApi().execute();
                    customDialog.dismiss();
                }
            }
        });

        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == mClear) {
                    signaturePad.clear();
                }            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
//
//        double d = screenWidth * MATCH_PARENT;
//        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            showSignatureAlertDialog();
        }
        else
        {
            Toast.makeText(SignatureActivity1.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
        }
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        JSONObject imageObj = new JSONObject();
        try {
            parentObj.put("Userid", userPrefs.getInt(KEY_USERID, 0));
            parentObj.put("Mobile", userPrefs.getString(KEY_PHONE, "0"));
            parentObj.put("FullName", userPrefs.getString(KEY_USERNAME, "0"));
            parentObj.put("Email", userPrefs.getString(KEY_EMAIL, "0"));
            parentObj.put("Image",null);
            parentObj.put("SignatureImage", image);

            parentObj.put("SignatureType",1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class updateProfileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            Constants.showLoadingDialog(SignatureActivity1.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignatureActivity1.this);
            APIInterface apiService =
                    ApiClient.getClient(3).create(APIInterface.class);

            Call<UpdateProfile> call = apiService.updateProfile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateProfile>() {
                @Override
                public void onResponse(Call<UpdateProfile> call, Response<UpdateProfile> response) {
                    if (response.isSuccessful()) {
                        UpdateProfile UpdateProfile = response.body();
                        Log.d("TAG", "onResponse: "+response.body());
                        try {
                            if (UpdateProfile.getStatus()) {
                                userPrefsEditor.putString(KEY_SIGNATURE, image);
                                userPrefsEditor.commit();
                                Toast.makeText(SignatureActivity1.this, UpdateProfile.getMessageEn(), Toast.LENGTH_SHORT).show();
                            } else {
                                String failureResponse = UpdateProfile.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), SignatureActivity1.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SignatureActivity1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignatureActivity1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<UpdateProfile> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SignatureActivity1.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignatureActivity1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}