package com.cs.mawthuq.Activitys;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.mawthuq.Adapters.DashboardDraftsAdapter;
import com.cs.mawthuq.Adapters.DraftsAdapter;
import com.cs.mawthuq.Fragment.PluseDailogBox;
import com.cs.mawthuq.Models.ChangePasswordResponce;
import com.cs.mawthuq.Models.DashboardResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DashBordActivity extends AppCompatActivity {

    TextView more, welcomeMessage, welcomeNotificationMessage, notificationCount, waiting_count, waiting_others_count, draftsText;
    RecyclerView draftsList;
    CardView chat1, chat2, chat3;
    TextView chat1Name, chat2Name, chat3Name;
    TextView chat1Count, chat2Count, chat3Count;
    ImageView chat1Image, chat2Image, chat3Image;
    RelativeLayout profilelayout, bottomLayout;
    LinearLayout chatlayout;
    RelativeLayout noChatLayout;
    ImageView pluse_btn, profilePic, history;
    FragmentManager fragmentManager = getSupportFragmentManager();
    PluseDailogBox newFragment;
    SharedPreferences userPrefs;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashbord_activity);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        more = (TextView) findViewById(R.id.more);
        welcomeMessage = (TextView) findViewById(R.id.daystatus);
        welcomeNotificationMessage = (TextView) findViewById(R.id.welcome_notification_text);
        notificationCount = (TextView) findViewById(R.id.notification_count);
        waiting_count = (TextView) findViewById(R.id.waiting_count);
        waiting_others_count = (TextView) findViewById(R.id.waiting_others_count);
        draftsText = (TextView) findViewById(R.id.drafts_text);

        chat1Name = (TextView) findViewById(R.id.chat1_name);
        chat2Name = (TextView) findViewById(R.id.chat2_name);
        chat3Name = (TextView) findViewById(R.id.chat3_name);

        chat1Count = (TextView) findViewById(R.id.chat1_count);
        chat2Count = (TextView) findViewById(R.id.chat2_count);
        chat3Count = (TextView) findViewById(R.id.chat3_count);

        chat1Image = (ImageView) findViewById(R.id.chat1_image);
        chat2Image = (ImageView) findViewById(R.id.chat2_image);
        chat3Image = (ImageView) findViewById(R.id.chat3_image);

        chat1 = (CardView) findViewById(R.id.chat1_layout);
        chat2 = (CardView) findViewById(R.id.chat2_layout);
        chat3 = (CardView) findViewById(R.id.chat3_layout);

        draftsList = (RecyclerView) findViewById(R.id.drafts_list);
        profilelayout = (RelativeLayout) findViewById(R.id.profilelayout);
        chatlayout = (LinearLayout) findViewById(R.id.chatlayout);
        noChatLayout = (RelativeLayout) findViewById(R.id.no_chat_layout);

        pluse_btn = (ImageView) findViewById(R.id.pluse);
        profilePic = (ImageView) findViewById(R.id.ac_profilepic);
        history = (ImageView) findViewById(R.id.history);

        RelativeLayout inboxLayout = (RelativeLayout) findViewById(R.id.inbox_layout);
        inboxLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashBordActivity.this, InboxActivity.class));
            }
        });

        RelativeLayout outboxLayout = (RelativeLayout) findViewById(R.id.outbox_layout);
        outboxLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashBordActivity.this, OutboxActivity.class));
            }
        });

        newFragment = PluseDailogBox.newInstance();

        pluse_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        chatlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBordActivity.this, ChatListActivity.class);
                startActivity(intent);
            }
        });
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBordActivity.this, DraftsActivity.class);
                startActivity(intent);
            }
        });
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBordActivity.this, MyProfileActivity.class);
                startActivity(intent);
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBordActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showDialog() {
        Bundle args = new Bundle();
        newFragment.setCancelable(true);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed
                if (newFragment != null) {
                    newFragment.dismiss();
                }
            }
        });
    }

    private void displayWelcomeMessage() {
        String currentHour = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
        if (Integer.parseInt(currentHour) >= 4 && Integer.parseInt(currentHour) < 12) {
            welcomeMessage.setText("Good Morning " + userPrefs.getString(KeyConstants.KEY_USERNAME, "") + " !");
        } else if (Integer.parseInt(currentHour) >= 12 && Integer.parseInt(currentHour) < 16) {
            welcomeMessage.setText("Good Afternoon " + userPrefs.getString(KeyConstants.KEY_USERNAME, "") + " !");
        } else if (Integer.parseInt(currentHour) >= 16 && Integer.parseInt(currentHour) < 20) {
            welcomeMessage.setText("Good Evening " + userPrefs.getString(KeyConstants.KEY_USERNAME, "") + " !");
        } else {
            welcomeMessage.setText("Good Night " + userPrefs.getString(KeyConstants.KEY_USERNAME, "") + " !");
        }

        Glide.with(this)
                .load(Constants.USERS_IMAGE_URL + userPrefs.getString(KeyConstants.KEY_PROFILE_PIC, ""))
                .placeholder(R.drawable.ic_profile_pic_round)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Animation zoomout = AnimationUtils.loadAnimation(DashBordActivity.this, R.anim.fade_scale_out);
                        profilePic.setAnimation(zoomout);
                        return false;
                    }
                })
                .into(profilePic);
    }

    private class getDashboardApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
            Constants.showLoadingDialog(DashBordActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(DashBordActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<DashboardResponse> call = apiService.userDashboard(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DashboardResponse>() {
                @Override
                public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                    if (response.isSuccessful()) {
                        DashboardResponse DashboardResponse = response.body();
                        Log.d("TAG", "onResponse: " + response.body());
                        try {
                            if (DashboardResponse.getStatus()) {
                                updateUI(DashboardResponse);
                            } else {
                                String failureResponse = DashboardResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), DashBordActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(DashBordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(DashBordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<DashboardResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(DashBordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DashBordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareSignUpJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getInt(KeyConstants.KEY_USERID, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private void updateUI(DashboardResponse response) {
        waiting_count.setText(response.getWatingForYouCount());
        waiting_others_count.setText(response.getWaitingForOtherCount());

        if (response.getNotificationCount() > 0) {
            notificationCount.setVisibility(View.VISIBLE);
            welcomeNotificationMessage.setText("You have unread notifications");
        } else {
            notificationCount.setVisibility(View.GONE);
            welcomeNotificationMessage.setText("Welcome Back");
        }

        DashboardDraftsAdapter mAdapter = new DashboardDraftsAdapter(this, response.getDraftDocument());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        draftsList.setLayoutManager(new GridLayoutManager(this, 1));
        draftsList.setAdapter(mAdapter);

        if (response.getDraftDocument().size() == 0) {
            draftsText.setVisibility(View.VISIBLE);
        }

        if (response.getUnreadMessages().size() > 0) {
            noChatLayout.setVisibility(View.GONE);
            chatlayout.setVisibility(View.VISIBLE);

            for (int i = 0; i < response.getUnreadMessages().size(); i++) {
                if (i == 0) {
                    chat1.setVisibility(View.VISIBLE);
                    chat1Name.setText(response.getUnreadMessages().get(i).getSenderName());
                    Glide.with(this)
                            .load(Constants.USERS_IMAGE_URL + response.getUnreadMessages().get(i).getProfileImage())
                            .placeholder(R.drawable.ic_profile_pic_round).into(chat1Image);

                    if (response.getUnreadMessages().get(i).getUnreadCount() > 0) {
                        chat1Count.setText("" + response.getUnreadMessages().get(i).getUnreadCount());
                        chat1Count.setVisibility(View.VISIBLE);
                    }

                    int finalI1 = i;
                    chat1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(DashBordActivity.this, ChatActivity.class);
                            intent.putExtra("DocumentId", response.getUnreadMessages().get(finalI1).getDocumentId());
                            startActivity(intent);
                        }
                    });
                } else if (i == 1) {
                    chat2.setVisibility(View.VISIBLE);
                    chat2Name.setText(response.getUnreadMessages().get(i).getSenderName());
                    Glide.with(this)
                            .load(Constants.USERS_IMAGE_URL + response.getUnreadMessages().get(i).getProfileImage())
                            .placeholder(R.drawable.ic_profile_pic_round).into(chat2Image);

                    if (response.getUnreadMessages().get(i).getUnreadCount() > 0) {
                        chat2Count.setText("" + response.getUnreadMessages().get(i).getUnreadCount());
                        chat2Count.setVisibility(View.VISIBLE);
                    }

                    int finalI = i;
                    chat2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(DashBordActivity.this, ChatActivity.class);
                            intent.putExtra("DocumentId", response.getUnreadMessages().get(finalI).getDocumentId());
                            startActivity(intent);
                        }
                    });
                } else if (i == 2) {
                    chat3.setVisibility(View.VISIBLE);
                    chat3Name.setText(response.getUnreadMessages().get(i).getSenderName());
                    Glide.with(this)
                            .load(Constants.USERS_IMAGE_URL + response.getUnreadMessages().get(i).getProfileImage())
                            .placeholder(R.drawable.ic_profile_pic_round).into(chat3Image);

                    if (response.getUnreadMessages().get(i).getUnreadCount() > 0) {
                        chat3Count.setText("" + response.getUnreadMessages().get(i).getUnreadCount());
                        chat3Count.setVisibility(View.VISIBLE);
                    }

                    int finalI = i;
                    chat3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(DashBordActivity.this, ChatActivity.class);
                            intent.putExtra("DocumentId", response.getUnreadMessages().get(finalI).getDocumentId());
//                            ActivityOptionsCompat options = ActivityOptionsCompat.
//                                    makeSceneTransitionAnimation(DashBordActivity.this, (View)chat1Name, "profile");
                            startActivity(intent);
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayWelcomeMessage();
        new getDashboardApi().execute();
    }
}
