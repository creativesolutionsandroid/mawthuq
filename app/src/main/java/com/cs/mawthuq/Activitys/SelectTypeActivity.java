package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;

public class SelectTypeActivity extends AppCompatActivity {

    RelativeLayout chooseTemplateLayout, scanLayout, writeNewLayout;
    TextView title;
    ImageView back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_type);

        title = (TextView) findViewById(R.id.title);
        chooseTemplateLayout = (RelativeLayout) findViewById(R.id.choose_template_layout);
        scanLayout = (RelativeLayout) findViewById(R.id.scan_layout);
        writeNewLayout = (RelativeLayout) findViewById(R.id.write_new_layout);
        back_btn = (ImageView) findViewById(R.id.bak_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (Constants.DOCUMENT_OPTION == KeyConstants.ID_LETTER) {
            title.setText("Create a Letter");
        } else {
            title.setText("Create a Contract");
        }

        chooseTemplateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SelectTypeActivity.this, TemplatesActivity.class));
            }
        });

        scanLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SelectTypeActivity.this, ScanActivity.class));
            }
        });

        writeNewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SelectTypeActivity.this, WriteANewActivity.class));
            }
        });
    }
}