package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.Constants.customDialog;

public class MyProfileActivity extends AppCompatActivity {

    ImageView back_btn, profilePic;
    TextView notifaction, profile, profiletap, logout, support, username, mobileNumber;
    LinearLayout notificationlayout, profilelayout, logoutlayout;
    View profilpurpul, notipurpul;
    RelativeLayout editlayout, templates_layout, signaturelayout, change_password_layout, contacts_layout, history_layout;
    String refresh;
    SharedPreferences userPrefs;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myprofile_activity);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        back_btn = (ImageView) findViewById(R.id.bak_btn);
        profilePic = (ImageView) findViewById(R.id.ac_profilepic);
        notifaction = (TextView) findViewById(R.id.notitext);
        username = (TextView) findViewById(R.id.username);
        mobileNumber = (TextView) findViewById(R.id.mobile_number);
        profile = (TextView) findViewById(R.id.profiletext);
        editlayout = (RelativeLayout) findViewById(R.id.edit_layout);
        templates_layout = (RelativeLayout) findViewById(R.id.templates_layout);
        notificationlayout = (LinearLayout) findViewById(R.id.notifilayotmain);
        signaturelayout = (RelativeLayout) findViewById(R.id.signaturelayout);
        contacts_layout = (RelativeLayout) findViewById(R.id.contacts_layout);
        history_layout = (RelativeLayout) findViewById(R.id.history_layout);
        change_password_layout = (RelativeLayout) findViewById(R.id.change_password_layout);
        profilelayout = (LinearLayout) findViewById(R.id.profilelayout);
        logoutlayout = (LinearLayout) findViewById(R.id.logoutlayout);
        profiletap = (TextView) findViewById(R.id.profiletap);
        logout = (TextView) findViewById(R.id.logout);
        support = (TextView) findViewById(R.id.support);
        notipurpul = (View) findViewById(R.id.notipurpul);
        profilpurpul = (View) findViewById(R.id.profilpurpul);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profilelayout.setVisibility(View.VISIBLE);
                logoutlayout.setVisibility(View.VISIBLE);
                profilpurpul.setVisibility(View.VISIBLE);
                notificationlayout.setVisibility(View.GONE);
                notipurpul.setVisibility(View.GONE);
            }
        });

        notifaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationlayout.setVisibility(View.VISIBLE);
                notipurpul.setVisibility(View.VISIBLE);
                profilelayout.setVisibility(View.GONE);
                logoutlayout.setVisibility(View.GONE);
                profilpurpul.setVisibility(View.GONE);
            }
        });
        profiletap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfileActivity.this, EditProfileActivity.class);
                startActivity(intent);
            }
        });
        change_password_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfileActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
        templates_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.DOCUMENT_OPTION = 0;
                Intent intent = new Intent(MyProfileActivity.this, TemplatesActivity.class);
                startActivity(intent);
            }
        });
        signaturelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfileActivity.this, SignatureActivity1.class);
                startActivity(intent);
            }
        });
        contacts_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfileActivity.this, ContactsActivity.class);
                startActivity(intent);
            }
        });
        history_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfileActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showtwoButtonsAlertDialog();
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGmail();
            }
        });
    }

    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MyProfileActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

//            title.setText("CkeckClik");
//            yes.setText(getResources().getString(R.string.yes));
//            no.setText(getResources().getString(R.string.no));
//            desc.setText("Do you want to logout?");

        desc.setText("Are you sure to logout?");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();

                Intent i = new Intent(MyProfileActivity.this, SignInActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });


        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        username.setText(userPrefs.getString(KeyConstants.KEY_USERNAME, ""));
        mobileNumber.setText("+" + userPrefs.getString(KeyConstants.KEY_PHONE, ""));

        Glide.with(this)
                .load(Constants.USERS_IMAGE_URL + userPrefs.getString(KeyConstants.KEY_PROFILE_PIC, ""))
                .placeholder(R.drawable.ic_profile_pic_round)
                .into(profilePic);
    }
    private void openGmail() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("plain/text");
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{""}); // Todo add gmail account
            i.putExtra(Intent.EXTRA_SUBJECT, "Mawthuq app feedback");
            i.putExtra(Intent.EXTRA_TITLE, "Mawthuq app feedback");
            final PackageManager pm = getPackageManager();
            final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
            String className = null;
            for (final ResolveInfo info : matches) {
                if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                    className = info.activityInfo.name;

                    if (className != null && !className.isEmpty()) {
                        break;
                    }
                }
            }
            i.setClassName("com.google.android.gm", className);
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
//                if (language.equalsIgnoreCase("En")){
                    Toast.makeText(MyProfileActivity.this, getResources().getString(R.string.there_are_no_email_apps_installed), Toast.LENGTH_SHORT).show();
//                }else {
//                    Toast.makeText(MyProfileActivity.this, getResources().getString(R.string.there_are_no_email_apps_installed_ar), Toast.LENGTH_SHORT).show();
//                }

            }
        } catch (Exception e) {
            e.printStackTrace();
//            if (language.equalsIgnoreCase("En")){
                Toast.makeText(MyProfileActivity.this, getResources().getString(R.string.there_are_no_email_apps_installed), Toast.LENGTH_SHORT).show();
//            }else {
//                Toast.makeText(MyProfileActivity.this, getResources().getString(R.string.there_are_no_email_apps_installed_ar), Toast.LENGTH_SHORT).show();
//            }

        }
    }
}
