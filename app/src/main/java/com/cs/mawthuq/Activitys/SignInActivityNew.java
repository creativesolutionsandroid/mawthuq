package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import com.cs.mawthuq.R;

import java.security.KeyStore;
import java.util.concurrent.Executor;

import javax.crypto.Cipher;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignInActivityNew extends Activity {

    EditText phonenumber;
    TextView signup;
    Button verfiy;
    String strphone,signin="signin";

    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "mawthuq";
    private Cipher cipher;
    private TextView textView;


    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);

        textView = (TextView) findViewById(R.id.errorText);

        phonenumber=(EditText)findViewById(R.id.edit_phone);
        signup=(TextView)findViewById(R.id.signup);
        verfiy=(Button)findViewById(R.id.verfiybtn);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(SignInActivityNew.this,SignUpActivity.class);
                startActivity(intent);
            }
        });
        verfiy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validations()){

                    Intent intent =new Intent(SignInActivityNew.this,DashBordActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });


        executor = ContextCompat.getMainExecutor(this);

//        biometricPrompt = new BiometricPrompt(this,
//                executor, new BiometricPrompt.AuthenticationCallback() {
//            @Override
//            public void onAuthenticationError(int errorCode,
//                                              @NonNull CharSequence errString) {
//                super.onAuthenticationError(errorCode, errString);
//                Toast.makeText(getApplicationContext(),
//                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
//                        .show();
//            }
//
//            @Override
//            public void onAuthenticationSucceeded(
//                    @NonNull BiometricPrompt.AuthenticationResult result) {
//                super.onAuthenticationSucceeded(result);
//                Toast.makeText(getApplicationContext(),
//                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAuthenticationFailed() {
//                super.onAuthenticationFailed();
//                Toast.makeText(getApplicationContext(), "Authentication failed",
//                        Toast.LENGTH_SHORT)
//                        .show();
//            }
//        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Biometric login for my app")
                .setSubtitle("Log in using your biometric credential")
                .setNegativeButtonText("Use account password")
                .build();

        biometricPrompt.authenticate(promptInfo);

    }

    private boolean validations(){
        strphone=phonenumber.getText().toString();

        if (strphone.length()==0){
            phonenumber.setError(getString(R.string.error_phone));
            return false;
        }
        if (strphone.length()<9){
            phonenumber.setError(getString(R.string.error_entervalidnumber));
            return false;
        }

        return true;
    }

}
