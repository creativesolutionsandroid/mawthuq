package com.cs.mawthuq.Activitys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cs.mawthuq.Models.ChangePasswordResponce;
import com.cs.mawthuq.Models.ChangePasswordResponce;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class ChangePasswordActivity extends AppCompatActivity {

    ImageView back_btn;
    Button done ;
    EditText oldpassword, newpassword , confirampassword ;
    String stroldpassword,  strnewpassword,strconfpassword;
    SharedPreferences userPrefs;
    Integer userid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        userid =userPrefs.getInt(KeyConstants.KEY_USERID, 0);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        done = (Button) findViewById(R.id.verfiybtn);
        oldpassword = (EditText) findViewById(R.id.old_password);
        newpassword = (EditText) findViewById(R.id.new_password);
        confirampassword = (EditText) findViewById(R.id.confiram_password);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPwd = oldpassword.getText().toString();
                String newPwd = newpassword.getText().toString();
                String confirmPwd = confirampassword.getText().toString();
                stroldpassword = oldPwd;
                strnewpassword = newPwd;
                if (oldPwd.length() == 0) {
                    oldpassword.setError("Please enter old password");
                } else if (newPwd.length() == 0) {
                    newpassword.setError("Please enter new password");
                } else if (newPwd.length() < 5) {
                    newpassword.setError(getString(R.string.invalid_password));
                } else if (confirmPwd.length() == 0) {
                    confirampassword.setError("Please retype password");
                } else if (!newPwd.equals(confirmPwd)) {
                    confirampassword.setError("Passwords not match, please retype");
                } else {

                    new ResetUpApi().execute();
                }
            }
        });

    }


    private class ResetUpApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
            Constants.showLoadingDialog(ChangePasswordActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePasswordActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<ChangePasswordResponce> call = apiService.userChangepassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangePasswordResponce>() {
                @Override
                public void onResponse(Call<ChangePasswordResponce> call, Response<ChangePasswordResponce> response) {
                    if (response.isSuccessful()) {
                        ChangePasswordResponce ChangePasswordResponce = response.body();
                        Log.d("TAG", "onResponse: "+response.body());
                        try {
                            if (ChangePasswordResponce.getStatus()) {

                                Toast.makeText(ChangePasswordActivity.this, ChangePasswordResponce.getMessageEn(), Toast.LENGTH_SHORT).show();
                                finish();

                            } else {
                                String failureResponse = ChangePasswordResponce.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ChangePasswordActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ChangePasswordResponce> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ChangePasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareSignUpJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("OldPassword",stroldpassword);
            parentObj.put("NewPassword",strnewpassword);
            parentObj.put("ConfirmPassword",strnewpassword);
            parentObj.put("UserId",userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }
}