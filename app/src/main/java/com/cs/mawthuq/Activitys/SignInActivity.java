package com.cs.mawthuq.Activitys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import com.cs.mawthuq.Models.SignUpResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executor;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_EMAIL;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PASSWORD;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PROFILE_PIC;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_ROLETYPE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_SIGNATURE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERNAME;

@RequiresApi(api = Build.VERSION_CODES.M)
public class SignInActivity extends AppCompatActivity {

    EditText phonenumber, password;
    TextView signup,forgotpassword;
    Button verfiy;
    String strphone, strPassword, signin = "signin";
    ImageView fingerimage;

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    SharedPreferences LanguagePrefs;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String TAG = "TAG";

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

//        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
//        fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
//
//        textView = (TextView) findViewById(R.id.errorText);
        phonenumber = (EditText) findViewById(R.id.edit_phone);
        password = (EditText) findViewById(R.id.edit_password);
        fingerimage = (ImageView) findViewById(R.id.fingerimage);
        signup = (TextView) findViewById(R.id.signup);
        forgotpassword = (TextView) findViewById(R.id.forgotpassword);
        verfiy = (Button) findViewById(R.id.verfiybtn);

        if (userPrefs.getInt(KeyConstants.KEY_USERID, 0) == 0) {
            fingerimage.setVisibility(View.INVISIBLE);
        }

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(SignInActivity.this,ForgotPassword.class);
                startActivity(intent);
            }
        });


        verfiy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validations()) {
                    new signInApi().execute();
                }
            }
        });

        fingerimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                biometricPrompt.authenticate(promptInfo);
            }
        });

        executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(SignInActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
//                Toast.makeText(getApplicationContext(),
//                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SignInActivity.this, DashBordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Mawthuq")
                .setSubtitle("Log in using your biometric credential")
                .setNegativeButtonText("Cancel")
                .build();

        BiometricManager biometricManager = BiometricManager.from(this);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Log.e("MY_APP_TAG", "No biometric features available on this device.");
                showErrorDialog();
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                showErrorDialog();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                Log.e("MY_APP_TAG", "The user hasn't associated " +
                        "any biometric credentials with their account.");
                showErrorDialog();
                break;
        }
    }

    private void showErrorDialog() {
        Constants.showOneButtonAlertDialog("App doesn't support this device. App require either Finger Print or Face Id enabled on device.", getResources().getString(R.string.app_name),
                getResources().getString(R.string.ok), SignInActivity.this);
        verfiy.setAlpha(0.5f);
//        verfiy.setEnabled(false);
        forgotpassword.setAlpha(0.5f);
        forgotpassword.setEnabled(false);
        signup.setAlpha(0.5f);
        signup.setEnabled(false);
    }

    private boolean validations() {
        strphone = phonenumber.getText().toString().trim();
        strPassword = password.getText().toString().trim();

        if (strphone.length() == 0) {
            phonenumber.setError(getString(R.string.error_phone));
            return false;
        }
        if (strphone.length() < 9) {
            phonenumber.setError(getString(R.string.error_entervalidnumber));
            return false;
        }
        if (strPassword.length() < 5) {
            password.setError(getString(R.string.error_password));
            return false;
        }
        return true;
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("PhoneNumber", "966" + strphone);
            parentObj.put("Password", strPassword);
            parentObj.put("Language", "En");
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("DeviceVersion", Constants.getDeviceType(this));
            parentObj.put("DeviceType", "Android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class signInApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            Constants.showLoadingDialog(SignInActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<SignUpResponse> call = apiService.userLogin(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                    if (response.isSuccessful()) {
                        SignUpResponse SignUpResponse = response.body();
                        Log.d(TAG, "onResponse: "+response.body());
                        try {
                            if (SignUpResponse.getStatus()) {
                                userPrefsEditor.putInt(KEY_USERID, SignUpResponse.getData().getUserId());
                                userPrefsEditor.putString(KEY_USERNAME, SignUpResponse.getData().getFullName());
                                userPrefsEditor.putString(KEY_PHONE, SignUpResponse.getData().getPhoneNumber());
                                userPrefsEditor.putInt(KEY_ROLETYPE, SignUpResponse.getData().getRoleId());
                                userPrefsEditor.putString(KEY_PROFILE_PIC, SignUpResponse.getData().getImageName());
                                userPrefsEditor.putString(KEY_EMAIL, SignUpResponse.getData().getEmail());
                                userPrefsEditor.putString(KEY_SIGNATURE, SignUpResponse.getData().getSignatureImage());
                                userPrefsEditor.commit();

//                                Toast.makeText(SignInActivity.this, SignUpResponse.getMessageEn(), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(SignInActivity.this, DashBordActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                String failureResponse = SignUpResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), SignInActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
