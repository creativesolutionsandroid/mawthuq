package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.mawthuq.Models.TemplateData;
import com.cs.mawthuq.Models.TemplateList;
import com.cs.mawthuq.R;
import com.gainwise.linker.LinkProfile;
import com.gainwise.linker.Linker;
import com.gainwise.linker.LinkerListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import static com.cs.mawthuq.Utils.Constants.isValidEmail;

public class SenderTemplateEditActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    TemplateList.Template template = new TemplateList.Template();
    String TAG = "TAG";
    String selectedText = "";
    TextView textView;
    ImageView back_btn;
    AlertDialog customDialog;
    private DatePickerDialog datePickerDialog;
    SharedPreferences userPrefs;

    InputFilter alphabetsfilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 ]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    InputFilter numbersfilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[1234567890.]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender_template_edit);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        template = (TemplateList.Template) getIntent().getSerializableExtra("template");

        TextView title = (TextView) findViewById(R.id.title);
        Button submitBtn = (Button) findViewById(R.id.btn_submit);
        textView = (TextView) findViewById(R.id.textView);
        back_btn = (ImageView) findViewById(R.id.bak_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean canSubmit = true;

                String jsonString = template.getTemplatejsonEn();
                TemplateData data = new TemplateData();
                Gson gson = new Gson();
                data = gson.fromJson(jsonString, TemplateData.class);
                List<TemplateData.ControlsArray> controls = data.getControls().getControls();

                for (int i = 0; i < controls.size(); i++) {
                    if (controls.get(i).getConfiguration().getRequiredBy().equalsIgnoreCase("Sender")) {
                        if (controls.get(i).getShoartCode().contains(controls.get(i).getControlName())) {
                            Toast.makeText(SenderTemplateEditActivity.this, "Please enter " + controls.get(i).getControlName(), Toast.LENGTH_SHORT).show();
                            canSubmit = false;
                            break;
                        }
                    }
                }

                if (canSubmit) {
                    Intent intent = new Intent(SenderTemplateEditActivity.this, SenderTemplateSubmitActivity.class);
                    intent.putExtra("template", template);
                    startActivity(intent);
                }
            }
        });

        title.setText(template.getTemplateNameEn());
        setData();
    }

    private void setData() {
        String templateText = "";
        String jsonString = template.getTemplatejsonEn(); //http request

        Log.d(TAG, "setData: "+jsonString);
        TemplateData data = new TemplateData();
        Gson gson = new Gson();
        data = gson.fromJson(jsonString, TemplateData.class);
        List<TemplateData.Blocks> blocks = data.getTemplate().getContent().get(0).getBlocks();
        for (int i = 0; i < blocks.size(); i++) {
            templateText = templateText + "\n" + blocks.get(i).getData().getText();
        }
        textView.setText(templateText);

        // setting links
        Linker linker = new Linker(textView);
        ArrayList<LinkProfile> profiles = new ArrayList<>();

        List<TemplateData.ControlsArray> controls = data.getControls().getControls();
        for (int i = 0; i < controls.size(); i++) {
            String link = controls.get(i).getShoartCode();
            if (controls.get(i).getConfiguration().getRequiredBy().equalsIgnoreCase("Sender")) {
                profiles.add(new LinkProfile(link,
                        Color.BLUE, true));
            } else {
                profiles.add(new LinkProfile(link,
                        Color.MAGENTA, true));
            }
        }
        linker.addProfiles(profiles);

        linker.setListener(new LinkerListener() {
            @Override
            public void onLinkClick(String charSequenceClicked) {
                // charSequenceClicked is the word that was clicked
                selectedText = charSequenceClicked;
                String jsonString = template.getTemplatejsonEn(); //http request
                Log.d(TAG, "jsonString: " + jsonString);
                TemplateData data = new TemplateData();
                Gson gson = new Gson();
                data = gson.fromJson(jsonString, TemplateData.class);
                List<TemplateData.ControlsArray> controls = data.getControls().getControls();
                for (int i = 0; i < controls.size(); i++) {
                    if (controls.get(i).getShoartCode().equals(selectedText)) {
                        if (controls.get(i).getConfiguration().getRequiredBy().equalsIgnoreCase("Sender")) {
                            if (controls.get(i).getConfiguration().getControltype().equalsIgnoreCase("Date")) {
                                showDatePicker(controls.get(i));
                            } else {
                                displayAlertDialog(controls.get(i));
                            }
                            break;
                        } else {
                            Toast.makeText(SenderTemplateEditActivity.this, "Only Receiver can update " + controls.get(i).getControlName(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        linker.update();
    }

    public void displayAlertDialog(final TemplateData.ControlsArray configuration) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SenderTemplateEditActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_template;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        boolean isEmailField = false;

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final EditText editText = (EditText) dialogView.findViewById(R.id.edittext);
        final AutoCompleteTextView spinner = (AutoCompleteTextView) dialogView.findViewById(R.id.spinner);
        final TextInputLayout spinnerLayout = (TextInputLayout) dialogView.findViewById(R.id.layout_spinner);
        final TextInputLayout editTextLayout = (TextInputLayout) dialogView.findViewById(R.id.input_edittext);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        final String[] updatedText = {""};

        title.setText(configuration.getControlName());
        if (!configuration.getShoartCode().contains(configuration.getControlName())) {
            updatedText[0] = configuration.getShoartCode();

            editText.setText(configuration.getShoartCode());
            editText.setSelection(editText.length());
            yes.setEnabled(true);
            yes.setAlpha(1.0f);

            spinner.setText(configuration.getShoartCode());
        }

        if (!configuration.getConfiguration().getControltype().equalsIgnoreCase("DropDown")) {
            editText.setHint(configuration.getConfiguration().getPlacehoder());
            if (configuration.getConfiguration().getControltype().equalsIgnoreCase("Text")) {
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                editText.setFilters(new InputFilter[]{alphabetsfilter, new InputFilter.LengthFilter(configuration.getConfiguration().getMaxlength())});
            } else if (configuration.getConfiguration().getControltype().equalsIgnoreCase("Number")) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                editText.setFilters(new InputFilter[]{numbersfilter, new InputFilter.LengthFilter(configuration.getConfiguration().getMaxNumber())});
            } else if (configuration.getConfiguration().getControltype().equalsIgnoreCase("Currency")) {
                editText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            } else if (configuration.getConfiguration().getControltype().equalsIgnoreCase("Email")) {
                editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                isEmailField = true;
            }

            final boolean finalIsEmailField = isEmailField;
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int minLenght = 0;
                    minLenght = Math.max(configuration.getConfiguration().getMinNumber(), configuration.getConfiguration().getMinLength());
                    if (editText.getText().toString().trim().length() >= minLenght) {
                        updatedText[0] = editText.getText().toString().trim();
                        if (!finalIsEmailField) {
                            yes.setEnabled(true);
                            yes.setAlpha(1.0f);
                        } else {
                            if (isValidEmail(updatedText[0])) {
                                yes.setEnabled(true);
                                yes.setAlpha(1.0f);
                            } else {
                                editText.setError("Please enter valid email");
                                yes.setEnabled(false);
                                yes.setAlpha(0.5f);
                            }
                        }
                    } else {
                        editText.setError("Minimum " + minLenght + " characters required.");
                        yes.setEnabled(false);
                        yes.setAlpha(0.5f);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } else {
            String[] options = configuration.getConfiguration().getOptions().split(",");
            final ArrayList<String> values = new ArrayList<>(Arrays.asList(options));
            spinner.setHint("Select " + configuration.getControlName());

            ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, values) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(12);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };
            spinner.setAdapter(cityAdapter);
            spinnerLayout.setVisibility(View.VISIBLE);
            editTextLayout.setVisibility(View.INVISIBLE);
            spinner.setInputType(InputType.TYPE_NULL);

            spinner.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return true;
                }
            });

            spinner.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    updatedText[0] = spinner.getText().toString().trim();
                    yes.setEnabled(true);
                    yes.setAlpha(1.0f);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (updatedText[0].length() > 0) {
                    String oldString = template.getTemplatejsonEn();
                    oldString = oldString.replace(selectedText, updatedText[0]);
                    template.setTemplatejsonEn(oldString);
                    setData();
                    customDialog.dismiss();
                } else {
                    Toast.makeText(SenderTemplateEditActivity.this, "Please select input", Toast.LENGTH_SHORT).show();
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void showDatePicker(final TemplateData.ControlsArray configuration) {
        datePickerDialog = DatePickerDialog.newInstance(SenderTemplateEditActivity.this);
        datePickerDialog.setThemeDark(false);
        datePickerDialog.showYearPickerFirst(false);
        datePickerDialog.setTitle(configuration.getControlName());

        // Setting Min Date to today date
        Calendar min_date_c = Calendar.getInstance();
        datePickerDialog.setMinDate(min_date_c);

        // Setting Max Date to next 2 years
        Calendar max_date_c = Calendar.getInstance();
//        max_date_c.add(Calendar.MONTH, 1);
//        datePickerDialog.setMaxDate(max_date_c);

        datePickerDialog.show(getFragmentManager(), "DatePickerDialog");

    }

    @Override
    public void onDateSet(DatePickerDialog view, int Year, int Month, int Day) {
        String date = "";
        if (Day <= 9 && Month < 9) {
            date = "0" + Day + "/0" + (Month + 1) + "/" + Year;
        } else if (Day <= 9) {
            date = "0" + Day + "/" + (Month + 1) + "/" + Year;
        } else if (Month < 9) {
            date = Day + "/0" + (Month + 1) + "/" + Year;
        } else {
            date = Day + "/" + (Month + 1) + "/" + Year;
        }

        String oldString = template.getTemplatejsonEn();
        oldString = oldString.replace(selectedText, date);
        template.setTemplatejsonEn(oldString);
        setData();
    }
}