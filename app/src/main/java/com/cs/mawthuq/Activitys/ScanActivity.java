package com.cs.mawthuq.Activitys;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.mawthuq.Adapters.ScanImagesAdapter;
import com.cs.mawthuq.Adapters.TemplatesListAdapter;
import com.cs.mawthuq.Listeners.ScanImageListener;
import com.cs.mawthuq.Listeners.TemplateFavListener;
import com.cs.mawthuq.Models.TemplateList;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.Constants;
import com.github.drjacky.imagepicker.ImagePicker;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.adapter.image.impl.GlideAdapter;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class ScanActivity extends AppCompatActivity {

    Button nextBtn;
    ImageView back_btn;
    String TAG = "TAG";
    RecyclerView imagesListView;
    ScanImagesAdapter mAdapter;
    private ArrayList<Bitmap> images = new ArrayList<>();
    private ScanImageListener listener;

    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    Bitmap thumbnail;
    private static final int CAMERA_REQUEST = 4;
    int selectedPosition = 0;
    String strFileName;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        nextBtn = (Button) findViewById(R.id.btn_next);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        imagesListView = (RecyclerView) findViewById(R.id.list_images);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (!canAccessCamera()) {
            requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
        }
        if (!canAccessStorage()) {
            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
        }

        images.add(null);

        listener = new ScanImageListener() {
            @Override
            public void onPlusClicked(int position) {
                selectedPosition = position;
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (canAccessCamera() && canAccessStorage()) {
//                        showImageSelectionAlertDialog();
                        openCamera();
                    }
                    if (!canAccessCamera()) {
                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                    }
                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    }
                } else {
//                    showImageSelectionAlertDialog();
                    openCamera();
                }
            }
        };

        mAdapter = new ScanImagesAdapter(ScanActivity.this, images,
                ScanActivity.this, listener);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ScanActivity.this);
        imagesListView.setLayoutManager(new GridLayoutManager(ScanActivity.this, 2));
        imagesListView.setAdapter(mAdapter);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                if (images.size() == 1) {
                    Constants.showOneButtonAlertDialog("Please add atleast one image.",
                            getString(R.string.app_name), getString(R.string.ok), ScanActivity.this);
                } else {
                    createPDF();
                }
            }
        });
    }

    private void openCamera() {
        String[] mimeTypes = {"image/png",
                "image/jpg",
                "image/jpeg"};
//        String[] mimeTypes = {"application/pdf"};

        ImagePicker.Companion.with(ScanActivity.this)
                .crop(3f, 4f)        //Crop image(Optional), Check Customization for more option
                .compress(1024)   //Final image size will be less than 1 MB(Optional)
                .maxResultSize(1122, 1122) //Final image resolution will be less than 1080 x 1080(Optional)
                .galleryMimeTypes(mimeTypes)
                .start();
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, perm));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Matrix matrix = new Matrix();
//            matrix.postRotate(270);

            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail, thumbnail.getWidth(),
                    thumbnail.getWidth(), true);
            thumbnail = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            images.set(selectedPosition, thumbnail);
            if (selectedPosition == images.size() - 1) {
                images.add(null);
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (canAccessCamera() && !canAccessStorage()) {
            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
        } else if (!canAccessCamera() && !canAccessStorage()) {
            Toast.makeText(this, getString(R.string.storage_permission_denied), Toast.LENGTH_SHORT).show();
        } else if (canAccessCamera() && canAccessStorage()) {
//            showImageSelectionAlertDialog();
            openCamera();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void createPDF() {
        String directoryPath = android.os.Environment.getExternalStorageDirectory().toString();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        strFileName = "SCAN_" + timeStamp + ".pdf";
        final File file = new File(directoryPath, strFileName);

        Constants.showLoadingDialog(this);

        new Thread(() -> {
            Bitmap bitmap;
            PdfDocument document = new PdfDocument();
            int height = 1122;
            int width = 794;

            for (int i = 0; i < images.size() - 1; i++) {

                bitmap = images.get(i);

                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);

                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(794, 1122, 1).create();
//                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(reqW, reqH, 1).create();
                PdfDocument.Page page = document.startPage(pageInfo);
                Canvas canvas = page.getCanvas();

                Log.e("PDF", "pdf = " + bitmap.getWidth() + "x" + bitmap.getHeight());
                canvas.drawBitmap(bitmap, 0, 0, null);

                document.finishPage(page);
            }

            FileOutputStream fos;
            try {
                fos = new FileOutputStream(file);
                document.writeTo(fos);
                document.close();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Constants.closeLoadingDialog();

        }).start();

        Constants.closeLoadingDialog();

        Intent intent = new Intent(ScanActivity.this, ScanSubmitActivity.class);
        intent.putExtra("file", strFileName);
        startActivity(intent);
    }
}