package com.cs.mawthuq.Activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.io.BufferedInputStream;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Adapters.InboxAdapter;
import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.Models.PDFData;
import com.cs.mawthuq.Models.TemplateData;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.gainwise.linker.LinkProfile;
import com.gainwise.linker.Linker;
import com.gainwise.linker.LinkerListener;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.util.Constants;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.Constants.isValidEmail;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class InboxTemplateEditActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,
        OnLoadCompleteListener, OnErrorListener {

    InboxResponse.Data template = new InboxResponse.Data();
    String TAG = "TAG";
    String selectedText = "";
    TextView textView;
    NestedScrollView nestedScrollView;
    ImageView back_btn, profile_pic, chat;
    AlertDialog customDialog;
    private DatePickerDialog datePickerDialog;
    SharedPreferences userPrefs;
    PDFView webView;
    TextView title, from, dateTextView, content;
    EditText etNote;

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; ++i) {
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 ]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                    return "";
                }
            }
            return null;
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_template_edit);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        template = (InboxResponse.Data) getIntent().getSerializableExtra("template");

        title = (TextView) findViewById(R.id.title);
        from = (TextView) findViewById(R.id.from);
        dateTextView = (TextView) findViewById(R.id.date);
        etNote = (EditText) findViewById(R.id.sender_note);
        content = (TextView) findViewById(R.id.content);

        Button submitBtn = (Button) findViewById(R.id.btn_submit);
        textView = (TextView) findViewById(R.id.textView);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        profile_pic = (ImageView) findViewById(R.id.profile_pic);
        chat = (ImageView) findViewById(R.id.chat);
        webView = (PDFView) findViewById(R.id.webivew);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nested_scrollview);

        enableFingerPrint();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InboxTemplateEditActivity.this, ChatActivity.class);
                intent.putExtra("DocumentId", template.getId());
                startActivity(intent);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean canSubmit = true;

                if (template.getTemplateOption() == KeyConstants.TYPE_TEMPLATE) {
                    String jsonString = template.getSendorJSON();
                    TemplateData data = new TemplateData();
                    Gson gson = new Gson();
                    data = gson.fromJson(jsonString, TemplateData.class);
                    List<TemplateData.ControlsArray> controls = data.getControls().getControls();

                    for (int i = 0; i < controls.size(); i++) {
                        if (controls.get(i).getConfiguration().getRequiredBy().equalsIgnoreCase("Receiver")) {
                            if (controls.get(i).getShoartCode().contains(controls.get(i).getControlName())) {
                                Toast.makeText(InboxTemplateEditActivity.this, "Please enter " + controls.get(i).getControlName(), Toast.LENGTH_SHORT).show();
                                canSubmit = false;
                                break;
                            }
                        }
                    }
                }

                if (etNote.getText().toString().trim().length() == 0) {
                    etNote.setError("please enter note");
                }
                else if (canSubmit) {
                    biometricPrompt.authenticate(promptInfo);
                }
            }
        });

        if (template.getTemplateOption() == KeyConstants.TYPE_TEMPLATE) {
            title.setText(template.getTemplateNameEn());
            setTemplateData();
            textView.setVisibility(View.VISIBLE);
        }
        else {
            nestedScrollView.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            setPDFData();
        }

        Glide.with(this)
                .load(com.cs.mawthuq.Utils.Constants.USERS_IMAGE_URL + template.getSenderImageName())
                .placeholder(R.drawable.ic_profile_pic_round)
                .into(profile_pic);

        from.setText("From: "+template.getSenderFullName());
        String date = template.getSentDate();
        date = date.replace("T", " ");
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date newDate = spf.parse(date);
            spf = new SimpleDateFormat("dd/MM/yyyy");
            date = spf.format(newDate);
            dateTextView.setText(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void enableFingerPrint() {
        executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(InboxTemplateEditActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
//                new insertSavedTemplateApi().execute();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                new insertSavedTemplateApi().execute();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Mawthuq")
                .setSubtitle("Authenticate finger print to Sign Document")
                .setNegativeButtonText("Cancel")
                .build();

        BiometricManager biometricManager = BiometricManager.from(this);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Log.e("MY_APP_TAG", "No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                Log.e("MY_APP_TAG", "The user hasn't associated " +
                        "any biometric credentials with their account.");
                break;
        }
    }

    private void setPDFData() {
        String jsonString = template.getSendorJSON();

        PDFData data = new PDFData();
        Gson gson = new Gson();
        data = gson.fromJson(jsonString, PDFData.class);

        title.setText(data.getDocumentName());

        Constants.Pinch.MINIMUM_ZOOM = 1f;
        Constants.Pinch.MAXIMUM_ZOOM = 1f;
        new showPDF().execute(com.cs.mawthuq.Utils.Constants.DOCUMENTS_URL + data.getDocumentName());

        if (template.getTemplateOption() == KeyConstants.TYPE_WRITE) {
            content.setVisibility(View.VISIBLE);
            content.setText("Content: "+data.getContent());
        }
    }

    @Override
    public void onError(Throwable t) {
        com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
        Toast.makeText(this, "Error displaying file", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadComplete(int nbPages) {
        com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
    }

    private class showPDF extends AsyncTask<String, Void, InputStream> {

        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream= null;
            try {
                URL uri = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) uri.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            com.cs.mawthuq.Utils.Constants.showLoadingDialog(InboxTemplateEditActivity.this);
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            super.onPostExecute(inputStream);
            webView.fromStream(inputStream)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(false)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .onLoad(InboxTemplateEditActivity.this)
                    .onError(InboxTemplateEditActivity.this)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    .spacing(0)
                    .autoSpacing(false) // add dynamic spacing to fit each page on its own on the screen
                    .pageFitPolicy(FitPolicy.BOTH) // mode to fit pages in the view
                    .fitEachPage(false) // fit each page to the view, else smaller pages are scaled relative to largest page.
                    .pageSnap(false) // snap pages to screen boundaries
                    .pageFling(false) // make a fling change only a single page like ViewPager
                    .nightMode(false) // toggle night mode
                    .load();
        }
    }


    private void setTemplateData() {
        String templateText = "";
        String jsonString = template.getSendorJSON(); //http request

        TemplateData data = new TemplateData();
        Gson gson = new Gson();
        data = gson.fromJson(jsonString, TemplateData.class);
        List<TemplateData.Blocks> blocks = data.getTemplate().getContent().get(0).getBlocks();
        for (int i = 0; i < blocks.size(); i++) {
            templateText = templateText + "\n" + blocks.get(i).getData().getText();
        }
        textView.setText(templateText);

        // setting links
        Linker linker = new Linker(textView);
        ArrayList<LinkProfile> profiles = new ArrayList<>();

        List<TemplateData.ControlsArray> controls = data.getControls().getControls();
        for (int i = 0; i < controls.size(); i++) {
            String link = controls.get(i).getShoartCode();
            if (controls.get(i).getConfiguration().getRequiredBy().equalsIgnoreCase("Receiver")) {
                profiles.add(new LinkProfile(link,
                        Color.BLUE, true));
            } else {
                profiles.add(new LinkProfile(link,
                        Color.MAGENTA, true));
            }
        }
        linker.addProfiles(profiles);

        linker.setListener(new LinkerListener() {
            @Override
            public void onLinkClick(String charSequenceClicked) {
                // charSequenceClicked is the word that was clicked
                selectedText = charSequenceClicked;
                String jsonString = template.getSendorJSON(); //http request
                Log.d(TAG, "jsonString: " + jsonString);
                TemplateData data = new TemplateData();
                Gson gson = new Gson();
                data = gson.fromJson(jsonString, TemplateData.class);
                List<TemplateData.ControlsArray> controls = data.getControls().getControls();
                for (int i = 0; i < controls.size(); i++) {
                    if (controls.get(i).getShoartCode().equals(selectedText)) {
                        if (controls.get(i).getConfiguration().getControltype().equalsIgnoreCase("Date")) {
                            showDatePicker(controls.get(i));
                        } else {
                            displayAlertDialog(controls.get(i));
                        }
                        break;
                    }
                }
            }
        });

        linker.update();
    }

    public void displayAlertDialog(final TemplateData.ControlsArray configuration) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(InboxTemplateEditActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_template;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        boolean isEmailField = false;

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final EditText editText = (EditText) dialogView.findViewById(R.id.edittext);
        final AutoCompleteTextView spinner = (AutoCompleteTextView) dialogView.findViewById(R.id.spinner);
        final TextInputLayout spinnerLayout = (TextInputLayout) dialogView.findViewById(R.id.layout_spinner);
        final TextInputLayout editTextLayout = (TextInputLayout) dialogView.findViewById(R.id.input_edittext);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        final String[] updatedText = {""};

        title.setText(configuration.getControlName());
        if (!configuration.getShoartCode().contains(configuration.getControlName())) {
            updatedText[0] = configuration.getShoartCode();

            editText.setText(configuration.getShoartCode());
            editText.setSelection(editText.length());
            yes.setEnabled(true);
            yes.setAlpha(1.0f);

            spinner.setText(configuration.getShoartCode());
        }

        if (!configuration.getConfiguration().getControltype().equalsIgnoreCase("DropDown")) {
            editText.setHint(configuration.getConfiguration().getPlacehoder());
            if (configuration.getConfiguration().getControltype().equalsIgnoreCase("Text")) {
                editText.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(configuration.getConfiguration().getMaxlength())});
            } else if (configuration.getConfiguration().getControltype().equalsIgnoreCase("Number")) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                editText.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(configuration.getConfiguration().getMaxNumber())});
            } else if (configuration.getConfiguration().getControltype().equalsIgnoreCase("Currency")) {
                editText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            } else if (configuration.getConfiguration().getControltype().equalsIgnoreCase("Email")) {
                editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                isEmailField = true;
            }

            final boolean finalIsEmailField = isEmailField;
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int minLenght = 0;
                    minLenght = Math.max(configuration.getConfiguration().getMinNumber(), configuration.getConfiguration().getMinLength());
                    if (editText.getText().toString().trim().length() >= minLenght) {
                        updatedText[0] = editText.getText().toString().trim();
                        if (!finalIsEmailField) {
                            yes.setEnabled(true);
                            yes.setAlpha(1.0f);
                        } else {
                            if (isValidEmail(updatedText[0])) {
                                yes.setEnabled(true);
                                yes.setAlpha(1.0f);
                            } else {
                                editText.setError("Please enter valid email");
                                yes.setEnabled(false);
                                yes.setAlpha(0.5f);
                            }
                        }
                    } else {
                        editText.setError("Minimum " + minLenght + " characters required.");
                        yes.setEnabled(false);
                        yes.setAlpha(0.5f);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } else {
            String[] options = configuration.getConfiguration().getOptions().split(",");
            final ArrayList<String> values = new ArrayList<>(Arrays.asList(options));
            spinner.setHint("Select " + configuration.getControlName());

            ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, values) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(12);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                    return v;
                }
            };
            spinner.setAdapter(cityAdapter);
            spinnerLayout.setVisibility(View.VISIBLE);
            editTextLayout.setVisibility(View.INVISIBLE);
            spinner.setInputType(InputType.TYPE_NULL);

            spinner.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return true;
                }
            });

            spinner.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    updatedText[0] = spinner.getText().toString().trim();
                    yes.setEnabled(true);
                    yes.setAlpha(1.0f);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (updatedText[0].length() > 0) {
                    String oldString = template.getSendorJSON();
                    oldString = oldString.replace(selectedText, updatedText[0]);
                    template.setSendorJSON(oldString);
                    setTemplateData();
                    customDialog.dismiss();
                } else {
                    Toast.makeText(InboxTemplateEditActivity.this, "Please select input", Toast.LENGTH_SHORT).show();
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void showDatePicker(final TemplateData.ControlsArray configuration) {
        datePickerDialog = DatePickerDialog.newInstance(InboxTemplateEditActivity.this);
        datePickerDialog.setThemeDark(false);
        datePickerDialog.showYearPickerFirst(false);
        datePickerDialog.setTitle(configuration.getControlName());

        Calendar min_date_c = Calendar.getInstance();
        datePickerDialog.setMinDate(min_date_c);

        datePickerDialog.show(getFragmentManager(), "DatePickerDialog");

    }

    @Override
    public void onDateSet(DatePickerDialog view, int Year, int Month, int Day) {
        String date = "";
        if (Day <= 9 && Month < 9) {
            date = "0" + Day + "/0" + (Month + 1) + "/" + Year;
        } else if (Day <= 9) {
            date = "0" + Day + "/" + (Month + 1) + "/" + Year;
        } else if (Month < 9) {
            date = Day + "/0" + (Month + 1) + "/" + Year;
        } else {
            date = Day + "/" + (Month + 1) + "/" + Year;
        }

        String oldString = template.getSendorJSON();
        oldString = oldString.replace(selectedText, date);
        template.setSendorJSON(oldString);
        setTemplateData();
    }

    private String prepareInsertJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("SignatureDocumentId", template.getId());
            parentObj.put("ReceiverId", template.getReceiverId());
            parentObj.put("ReceiverNotes", etNote.getText().toString().trim());
            parentObj.put("SignatureType", 1);


            if (template.getTemplateOption() == KeyConstants.TYPE_TEMPLATE) {
                String jsonString = template.getSendorJSON();
                TemplateData data = new TemplateData();
                Gson gson = new Gson();
                data = gson.fromJson(jsonString, TemplateData.class);
                List<TemplateData.ControlsArray> controls = data.getControls().getControls();
                for (int i = 0; i < controls.size(); i++) {
                    controls.get(i).setShoartCode("{{" + controls.get(i).getControlName() + "}}");
                }
                data.getControls().setControls(controls);
                String json = gson.toJson(data);
                try {
                    JSONObject obj = new JSONObject(json);
                    parentObj.put("ReceiverJSON", obj);
                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
                }
            }
            else {
                String jsonString = template.getSendorJSON();

                PDFData data = new PDFData();
                Gson gson = new Gson();
                data = gson.fromJson(jsonString, PDFData.class);
                String json = gson.toJson(data);
                try {
                    JSONObject obj = new JSONObject(json);
                    parentObj.put("ReceiverJSON", obj);
                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class insertSavedTemplateApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareInsertJson();
            com.cs.mawthuq.Utils.Constants.showLoadingDialog(InboxTemplateEditActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(InboxTemplateEditActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(com.cs.mawthuq.Utils.Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<BasicResponse> call = apiService.ReceiverUpdateSignatureDocument(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    if (response.isSuccessful()) {
                        BasicResponse BasicResponse = response.body();
                        try {
                            if (BasicResponse.isStatus()) {
//                                Toast.makeText(InboxTemplateEditActivity.this, BasicResponse.MessageEn, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(InboxTemplateEditActivity.this, DashBordActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                com.cs.mawthuq.Utils.Constants.showOneButtonAlertDialogWithIntent(BasicResponse.MessageEn, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), InboxTemplateEditActivity.this, intent);
                            } else {
                                String failureResponse = BasicResponse.getMessageEn();
                                com.cs.mawthuq.Utils.Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), InboxTemplateEditActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(InboxTemplateEditActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(InboxTemplateEditActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(InboxTemplateEditActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(InboxTemplateEditActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}