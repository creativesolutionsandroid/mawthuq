package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.mawthuq.Models.ValidateMobileResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;

public class ForgotPassword extends AppCompatActivity {

    ImageView back_btn;
    EditText mobile_number;
    Button done ;
   String strmobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.forgotpassword_activity);

        back_btn =(ImageView)findViewById(R.id.back_btn);
        mobile_number =(EditText) findViewById(R.id.phone);
        done =(Button) findViewById(R.id.forgot_submit_button);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPwd = mobile_number.getText().toString();

                strmobile = oldPwd;

                if (oldPwd.length() == 0) {
                    mobile_number.setError("Please enter mobile number");
                } else if (oldPwd.length() <9) {
                    mobile_number.setError("Please enter valid mobile number");
                }else {
                    new verifyMobileApi().execute();
                }
                
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(ForgotPassword.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPassword.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<ValidateMobileResponse> call = apiService.forgotvalidateMobile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ValidateMobileResponse>() {
                @Override
                public void onResponse(Call<ValidateMobileResponse> call, Response<ValidateMobileResponse> response) {
                    if (response.isSuccessful()) {
                        ValidateMobileResponse ValidateMobileResponse = response.body();
                        try {
                            if (ValidateMobileResponse.getStatus()) {
                                Log.d("TAG", "otp: "+ValidateMobileResponse.Data.OtpCode);
                                Intent intent = new Intent(ForgotPassword.this,VerifyOtpActivity.class);
                                    intent.putExtra("mobile", strmobile);
                                    intent.putExtra("otp", ValidateMobileResponse.Data.OtpCode);
                                    intent.putExtra("forgotpassword", true);
                                    startActivity(intent);
                            } else {
                                String failureResponse = ValidateMobileResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ForgotPassword.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ForgotPassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ForgotPassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<ValidateMobileResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ForgotPassword.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ForgotPassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }


    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("PhoneNumber", "966" +strmobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }


}