package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import com.cs.mawthuq.Models.SignUpResponse;
import com.cs.mawthuq.Models.ValidateMobileResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executor;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.content.ContentValues.TAG;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_EMAIL;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PASSWORD;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_ROLETYPE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERNAME;

public class VerifyOtpActivity extends AppCompatActivity {

    EditText inputotp;
    String strotp;
    Button verify_btn;
    String phonenumber;
    Boolean screen;
    TextView number, resendOtp;
    ImageView back_btn;
    SharedPreferences LanguagePrefs;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String TAG = "TAG";
    String otp, mobilenumber;


    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;


//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_otp_activity);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        otp = getIntent().getStringExtra("otp");
        Log.d(TAG, "onCreateotp: " + otp);
        mobilenumber = getIntent().getStringExtra("mobile");

        screen = getIntent().getExtras().getBoolean("forgotpassword");

        inputotp = (EditText) findViewById(R.id.inputotp_et);
        verify_btn = (Button) findViewById(R.id.verfiybtn);
        number = (TextView) findViewById(R.id.otpnumber);
        resendOtp = (TextView) findViewById(R.id.resend_otp);
        back_btn = (ImageView) findViewById(R.id.bak_btn);

        phonenumber = getIntent().getStringExtra(KEY_PHONE);

        number.setText("OTP code has been sent to your phone number 966" + phonenumber);
        if (screen) {
            number.setText("OTP code has been sent to your phone number 966" + mobilenumber);
        }
        
        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validations()) {
                    if (screen) {
                        if (!strotp.equals(otp)) {
                            inputotp.setError(getString(R.string.error_vaild_otp));
                        } else {
                            Log.d(TAG, "otp: " + otp);
                            Intent intent = new Intent(VerifyOtpActivity.this, ResetPasswordActivity.class);
                            intent.putExtra("otp", otp);
                            intent.putExtra("mobile", mobilenumber);
                            startActivity(intent);
                        }
                    } else {
                        biometricPrompt.authenticate(promptInfo);
                    }
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (screen) {
                    new forgotPassswordResendApi().execute();
                }
                else {
                    new verifyMobileApi().execute();
                }
            }
        });

        executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(VerifyOtpActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
//                new signUpApi().execute();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
//                Toast.makeText(getApplicationContext(),
//                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();
                new signUpApi().execute();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Mawthuq")
                .setSubtitle("Log in using your biometric credential")
                .setNegativeButtonText("Cancel")
                .build();

        BiometricManager biometricManager = BiometricManager.from(this);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Log.e("MY_APP_TAG", "No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                Log.e("MY_APP_TAG", "The user hasn't associated " +
                        "any biometric credentials with their account.");
                break;
        }


    }

    private boolean validations() {
        strotp = inputotp.getText().toString();

        if (strotp.length() == 0) {
            inputotp.setError(getString(R.string.error_otp));
            return false;
        }
        if (strotp.length() < 6) {
            inputotp.setError(getString(R.string.error_vaild_otp));
            return false;
        }

        return true;
    }

    private String prepareSignUpJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("FullName", getIntent().getStringExtra(KEY_USERNAME));
            parentObj.put("PhoneNumber", "966" + getIntent().getStringExtra(KEY_PHONE));
            parentObj.put("Password", getIntent().getStringExtra(KEY_PASSWORD));
            parentObj.put("OTP", strotp);
            parentObj.put("SignatureType", 1);
            parentObj.put("Language", "En");
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("DeviceVersion", Constants.getDeviceType(this));
            parentObj.put("DeviceType", "Android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class signUpApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
            Constants.showLoadingDialog(VerifyOtpActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<SignUpResponse> call = apiService.userSignUp(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                    if (response.isSuccessful()) {
                        SignUpResponse SignUpResponse = response.body();
                        Log.d(TAG, "onResponse: " + response.body());
                        try {
                            if (SignUpResponse.getStatus()) {
                                userPrefsEditor.putInt(KEY_USERID, SignUpResponse.getData().getUserId());
                                userPrefsEditor.putString(KEY_USERNAME, SignUpResponse.getData().getFullName());
                                userPrefsEditor.putString(KEY_EMAIL, SignUpResponse.getData().getEmail());
                                userPrefsEditor.putInt(KEY_ROLETYPE, SignUpResponse.getData().getRoleId());
                                userPrefsEditor.commit();

//                                Toast.makeText(VerifyOtpActivity.this, SignUpResponse.getMessageEn(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(VerifyOtpActivity.this, DashBordActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
                                Constants.showOneButtonAlertDialogWithIntent(SignUpResponse.getMessageEn(), getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), VerifyOtpActivity.this, intent);

                            } else {
                                String failureResponse = SignUpResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), VerifyOtpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VerifyOtpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void enableFingerPrint() {
    }


    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            if (screen) {
                parentObj.put("PhoneNumber", "966" + mobilenumber);
            } else {
                parentObj.put("PhoneNumber", "966" + getIntent().getStringExtra(KEY_PHONE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(VerifyOtpActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<ValidateMobileResponse> call = apiService.validateMobile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ValidateMobileResponse>() {
                @Override
                public void onResponse(Call<ValidateMobileResponse> call, Response<ValidateMobileResponse> response) {
                    if (response.isSuccessful()) {
                        ValidateMobileResponse ValidateMobileResponse = response.body();
                        try {
                            if (ValidateMobileResponse.getStatus()) {
                                Toast.makeText(VerifyOtpActivity.this, ValidateMobileResponse.MessageEn, Toast.LENGTH_SHORT).show();
                            } else {
                                String failureResponse = ValidateMobileResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), VerifyOtpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<ValidateMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VerifyOtpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class forgotPassswordResendApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(VerifyOtpActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<ValidateMobileResponse> call = apiService.forgotvalidateMobile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ValidateMobileResponse>() {
                @Override
                public void onResponse(Call<ValidateMobileResponse> call, Response<ValidateMobileResponse> response) {
                    if (response.isSuccessful()) {
                        ValidateMobileResponse ValidateMobileResponse = response.body();
                        try {
                            if (ValidateMobileResponse.getStatus()) {
                                Toast.makeText(VerifyOtpActivity.this, ValidateMobileResponse.MessageEn, Toast.LENGTH_SHORT).show();
                            } else {
                                String failureResponse = ValidateMobileResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), VerifyOtpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<ValidateMobileResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VerifyOtpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(VerifyOtpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
