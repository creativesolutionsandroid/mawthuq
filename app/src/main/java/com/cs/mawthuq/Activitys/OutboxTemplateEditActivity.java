package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.Models.PDFData;
import com.cs.mawthuq.Models.TemplateData;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Utils.KeyConstants;
import com.gainwise.linker.LinkProfile;
import com.gainwise.linker.Linker;
import com.gainwise.linker.LinkerListener;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.util.Constants;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OutboxTemplateEditActivity extends AppCompatActivity implements OnLoadCompleteListener, OnErrorListener {

    InboxResponse.Data template = new InboxResponse.Data();
    String TAG = "TAG";
    String selectedText = "";
    TextView textView;
    NestedScrollView nestedScrollView;
    ImageView back_btn, profile_pic, chat;
    AlertDialog customDialog;
    private DatePickerDialog datePickerDialog;
    SharedPreferences userPrefs;
    PDFView webView;
    TextView title, from, dateTextView, content;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outbox_template_edit);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        template = (InboxResponse.Data) getIntent().getSerializableExtra("template");

        title = (TextView) findViewById(R.id.title);
        from = (TextView) findViewById(R.id.from);
        dateTextView = (TextView) findViewById(R.id.date);
        content = (TextView) findViewById(R.id.content);
        chat = (ImageView) findViewById(R.id.chat);

        Button submitBtn = (Button) findViewById(R.id.btn_submit);
        textView = (TextView) findViewById(R.id.textView);
        profile_pic = (ImageView) findViewById(R.id.profile_pic);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        webView = (PDFView) findViewById(R.id.webivew);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nested_scrollview);

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OutboxTemplateEditActivity.this, ChatActivity.class);
                intent.putExtra("DocumentId", template.getId());
                startActivity(intent);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (template.getTemplateOption() == KeyConstants.TYPE_TEMPLATE) {
            title.setText(template.getTemplateNameEn());
            setTemplateData();
            textView.setVisibility(View.VISIBLE);
        }
        else {
            nestedScrollView.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            setPDFData();
        }

        Glide.with(this)
                .load(com.cs.mawthuq.Utils.Constants.USERS_IMAGE_URL + template.getReceiverImageName())
                .placeholder(R.drawable.ic_profile_pic_round)
                .into(profile_pic);

        from.setText("To: "+template.getReceiverFullName());
        String date = template.getSentDate();
        date = date.replace("T", " ");
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date newDate = spf.parse(date);
            spf = new SimpleDateFormat("dd/MM/yyyy");
            date = spf.format(newDate);
            dateTextView.setText(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPDFData() {
        String jsonString = template.getSendorJSON();

        PDFData data = new PDFData();
        Gson gson = new Gson();
        data = gson.fromJson(jsonString, PDFData.class);

        title.setText(data.getDocumentName());

        Constants.Pinch.MINIMUM_ZOOM = 1f;
        Constants.Pinch.MAXIMUM_ZOOM = 1f;
        new OutboxTemplateEditActivity.showPDF().execute(com.cs.mawthuq.Utils.Constants.DOCUMENTS_URL + data.getDocumentName());

        if (template.getTemplateOption() == KeyConstants.TYPE_WRITE) {
            content.setVisibility(View.VISIBLE);
            content.setText("Content: "+data.getContent());
        }
    }

    @Override
    public void onError(Throwable t) {
        com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
        Toast.makeText(this, "Error displaying file", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadComplete(int nbPages) {
        com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
    }

    private class showPDF extends AsyncTask<String, Void, InputStream> {

        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream= null;
            try {
                URL uri = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) uri.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            com.cs.mawthuq.Utils.Constants.showLoadingDialog(OutboxTemplateEditActivity.this);
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            super.onPostExecute(inputStream);
            webView.fromStream(inputStream)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(false)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .onLoad(OutboxTemplateEditActivity.this)
                    .onError(OutboxTemplateEditActivity.this)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    .spacing(0)
                    .autoSpacing(false) // add dynamic spacing to fit each page on its own on the screen
                    .pageFitPolicy(FitPolicy.BOTH) // mode to fit pages in the view
                    .fitEachPage(false) // fit each page to the view, else smaller pages are scaled relative to largest page.
                    .pageSnap(false) // snap pages to screen boundaries
                    .pageFling(false) // make a fling change only a single page like ViewPager
                    .nightMode(false) // toggle night mode
                    .load();
        }
    }

    private void setTemplateData() {
        String templateText = "";
        String jsonString = template.getSendorJSON(); //http request
        Log.d(TAG, "jsonString: "+jsonString);
        TemplateData data = new TemplateData();
        Gson gson = new Gson();
        data = gson.fromJson(jsonString, TemplateData.class);
        List<TemplateData.Blocks> blocks = data.getTemplate().getContent().get(0).getBlocks();
        for (int i = 0; i < blocks.size(); i++) {
            templateText = templateText + "\n" + blocks.get(i).getData().getText();
        }
        textView.setText(templateText);

        // setting links
        Linker linker = new Linker(textView);
        ArrayList<LinkProfile> profiles = new ArrayList<>();

        List<TemplateData.ControlsArray> controls = data.getControls().getControls();
        for (int i = 0; i < controls.size(); i++) {
            String link = controls.get(i).getShoartCode();
            if (controls.get(i).getConfiguration().getRequiredBy().equalsIgnoreCase("Receiver")) {
                profiles.add(new LinkProfile(link,
                        Color.BLUE, true));
            } else {
                profiles.add(new LinkProfile(link,
                        Color.MAGENTA, true));
            }
        }
        linker.addProfiles(profiles);
        linker.update();
    }
}