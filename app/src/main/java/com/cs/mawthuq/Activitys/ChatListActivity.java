package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.cs.mawthuq.R;

import java.nio.file.FileStore;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatListActivity extends Activity {

    ImageView back_btn;
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatlist_activity);
        back_btn=(ImageView)findViewById(R.id.bak_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
