package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.cs.mawthuq.Adapters.InboxViewPagerAdapter;
import com.cs.mawthuq.Fragment.DraftsFragment;
import com.cs.mawthuq.Fragment.InboxLetterFragment;
import com.cs.mawthuq.Models.Drafts;
import com.cs.mawthuq.Models.Drafts;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.ID_LETTER;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class DraftsActivity extends AppCompatActivity {

    ImageView back_btn;
    TabLayout tabs;
    ViewPager mViewPager;
    InboxViewPagerAdapter adapter;
    SharedPreferences userPrefs;
    private ArrayList<Drafts.Data> drafts = new ArrayList<>();
    private ArrayList<Drafts.Data> letters = new ArrayList<>();
    private ArrayList<Drafts.Data> contracts = new ArrayList<>();

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drafts_activity);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        back_btn=(ImageView)findViewById(R.id.bak_btn);
        tabs = (TabLayout) findViewById(R.id.tabs);
        mViewPager = (ViewPager) findViewById(R.id.container);

        setViewPager();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });

        new getDraftsApi().execute();

    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("SenderId", userPrefs.getInt(KEY_USERID, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class getDraftsApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            Constants.showLoadingDialog(DraftsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(DraftsActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<Drafts> call = apiService.GetDrafts(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Drafts>() {
                @Override
                public void onResponse(Call<Drafts> call, Response<Drafts> response) {
                    if (response.isSuccessful()) {
                        Drafts Drafts = response.body();
                        Log.d("TAG", "onResponse: " + response.body());
                        try {
                            if (Drafts.getStatus()) {
                                drafts = Drafts.getData();

                                filterData();
                            } else {
                                String failureResponse = Drafts.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), DraftsActivity.this);
                                setViewPager();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(DraftsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(DraftsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<Drafts> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(DraftsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DraftsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void filterData() {
        for (int i = 0; i < drafts.size(); i++) {
            if (drafts.get(i).getTemplateType() == ID_LETTER) {
                letters.add(drafts.get(i));
            }
            else {
                contracts.add(drafts.get(i));
            }
        }
        setViewPager();
    }

    private void setViewPager() {

        Constants.closeLoadingDialog();
        adapter = new InboxViewPagerAdapter(getSupportFragmentManager());

        DraftsFragment letterFragment = new DraftsFragment();
        Bundle letterBundle = new Bundle();
        letterBundle.putSerializable("inbox",letters);
        letterFragment.setArguments(letterBundle);

        DraftsFragment contractFragment = new DraftsFragment();
        Bundle contractBundle = new Bundle();
        contractBundle.putSerializable("inbox",contracts);
        contractFragment.setArguments(contractBundle);

        adapter.addFragment(letterFragment, "Letters");
        adapter.addFragment(contractFragment, "Contracts");

        mViewPager.setAdapter(adapter);
        tabs.setupWithViewPager(mViewPager);

    }

}
