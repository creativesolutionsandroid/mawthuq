package com.cs.mawthuq.Activitys;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.mawthuq.Models.UpdateProfile;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.github.drjacky.imagepicker.ImagePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_EMAIL;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PROFILE_PIC;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_SIGNATURE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERNAME;

public class EditProfileActivity extends Activity {

    ImageView back_btn, editicon, profilePic;
    TextView save, phonenumber;
    EditText name, email;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String strusername, strphone, strEmail, strFileName = "", strBase64FileData = "";
    AlertDialog customDialog;

    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    boolean isCamera = false;
    Bitmap thumbnail;
    Boolean isImageChanged = false;
    Uri imageUri;
    String imagePath;
    private byte[] image1Bytes;
    private static final int CAMERA_REQUEST = 4;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_activity);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        back_btn = (ImageView) findViewById(R.id.bak_btn);
        profilePic = (ImageView) findViewById(R.id.ac_profilepic);
        editicon = (ImageView) findViewById(R.id.editicon);
        save = (TextView) findViewById(R.id.save);
        phonenumber = (TextView) findViewById(R.id.edit_phone);
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);

        name.setText(userPrefs.getString(KeyConstants.KEY_USERNAME, ""));
        email.setText(userPrefs.getString(KeyConstants.KEY_EMAIL, ""));
        phonenumber.setText("+" + userPrefs.getString(KeyConstants.KEY_PHONE, ""));
//        strFileName = userPrefs.getString(KEY_PROFILE_PIC, "");
        name.setSelection(name.length());

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (!userPrefs.getString(KeyConstants.KEY_PROFILE_PIC, "").equalsIgnoreCase("")) {
            Glide.with(this)
                    .load(Constants.USERS_IMAGE_URL + userPrefs.getString(KeyConstants.KEY_PROFILE_PIC, ""))
                    .placeholder(R.drawable.ic_profile_pic_round)
                    .into(profilePic);
        }

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (canAccessCamera() && canAccessStorage()) {
                        openCamera();
                    }
                    if (!canAccessCamera()) {
                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                    }
                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    }
                }
                else {
                    openCamera();
                }
            }
        });

        save.setVisibility(View.VISIBLE);
        save.setEnabled(true);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validations()) {
                    new signInApi().execute();
                }
            }
        });
    }

    private void openCamera() {
        String[] mimeTypes = {"image/png",
                "image/jpg",
                "image/jpeg"};
//        String[] mimeTypes = {"application/pdf"};

        ImagePicker.Companion.with(EditProfileActivity.this)
                .cropOval()
                .crop(3f, 4f)        //Crop image(Optional), Check Customization for more option
                .compress(1024)   //Final image size will be less than 1 MB(Optional)
                .maxResultSize(1122, 1122) //Final image resolution will be less than 1080 x 1080(Optional)
                .galleryMimeTypes(mimeTypes)
                .start();
    }

    private boolean Validations() {
        strusername = name.getText().toString().trim();
        strphone = phonenumber.getText().toString().trim();
        strEmail = email.getText().toString().trim();

        if (strusername.length() == 0) {
            name.setError(getString(R.string.error_username));
            Constants.requestEditTextFocus(name, EditProfileActivity.this);
            return false;
        }
        if (strphone.length() == 0) {
            phonenumber.setError(getString(R.string.error_phone));
            return false;
        }
        if (strphone.length() < 9) {
            phonenumber.setError(getString(R.string.error_entervalidnumber));
            return false;
        }
        if (strEmail.length() == 0) {
            email.setError(getString(R.string.error_email));
            return false;
        }
        if (!Constants.isValidEmail(strEmail)) {
            email.setError(getString(R.string.error_vaild_email));
            return false;
        }
        return true;
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        JSONObject imageObj = new JSONObject();
        try {
            parentObj.put("Userid", userPrefs.getInt(KEY_USERID, 0));
            parentObj.put("Mobile", userPrefs.getString(KEY_PHONE, "0"));
            parentObj.put("FullName", strusername);
            parentObj.put("Email", strEmail);
            parentObj.put("SignatureImage", null);

            if (!strFileName.equalsIgnoreCase("")) {
                imageObj.put("FileName", strFileName);
                imageObj.put("Base64FileData", strBase64FileData);
                imageObj.put("FileUploadLocation", "/ProfilePicture/");
                imageObj.put("FileURLLocation", "");
                parentObj.put("Image", imageObj);
            }
            else {
                parentObj.put("Image", null);
            }
            parentObj.put("SignatureType",1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class signInApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            Constants.showLoadingDialog(EditProfileActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(3).create(APIInterface.class);

            Call<UpdateProfile> call = apiService.updateProfile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateProfile>() {
                @Override
                public void onResponse(Call<UpdateProfile> call, Response<UpdateProfile> response) {
                    if (response.isSuccessful()) {
                        UpdateProfile UpdateProfile = response.body();
                        Log.d("TAG", "onResponse: "+response.body());
                        try {
                            if (UpdateProfile.getStatus()) {
                                userPrefsEditor.putInt(KEY_USERID, UpdateProfile.getData().get(0).getUserId());
                                userPrefsEditor.putString(KEY_USERNAME, UpdateProfile.getData().get(0).getFullName());
                                userPrefsEditor.putString(KEY_PHONE, UpdateProfile.getData().get(0).getPhoneNumber());
                                userPrefsEditor.putString(KEY_PROFILE_PIC, UpdateProfile.getData().get(0).getImageName());
                                userPrefsEditor.putString(KEY_EMAIL, UpdateProfile.getData().get(0).getEmail());
                                userPrefsEditor.commit();
                                finish();
                                Toast.makeText(EditProfileActivity.this, UpdateProfile.getMessageEn(), Toast.LENGTH_SHORT).show();
                            } else {
                                String failureResponse = UpdateProfile.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), EditProfileActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UpdateProfile> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, perm));
    }

    public String StoreByteImage() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        strFileName = "ANDR_" + userPrefs.getInt(KEY_USERID,0) + "_" + timeStamp + ".jpg";

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, stream);

        image1Bytes = stream.toByteArray();
        strBase64FileData = Base64.encodeToString(image1Bytes, Base64.DEFAULT);

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "Mawthuq");
        folder.mkdir();

        File sdImageMainDirectory = new File(folder, strFileName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            thumbnail.compress(Bitmap.CompressFormat.PNG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            convertCameraImages();
        }
    }

    private void convertCameraImages() {
//        String path = getRealPathFromURI(imageUri);
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        bmOptions.inScaled = false;
//
//        isImageChanged = true;
//
//        thumbnail = BitmapFactory.decodeFile(path, bmOptions);
        Matrix matrix = new Matrix();
//        matrix.postRotate(90);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail, thumbnail.getWidth(),
                thumbnail.getHeight(), true);
        thumbnail = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        profilePic.setImageBitmap(thumbnail);
        imagePath = StoreByteImage();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (canAccessCamera() && !canAccessStorage()) {
            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
        }
        else if (!canAccessCamera() && !canAccessStorage()) {
            Toast.makeText(this, getString(R.string.storage_permission_denied), Toast.LENGTH_SHORT).show();
        }
        else if (canAccessCamera() && canAccessStorage()) {
            openCamera();
        }
    }
}
