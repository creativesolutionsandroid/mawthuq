package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.Drafts;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.Models.PDFData;
import com.cs.mawthuq.Models.TemplateData;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.gainwise.linker.LinkProfile;
import com.gainwise.linker.Linker;
import com.gainwise.linker.LinkerListener;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.util.Constants;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class DraftSendActivity extends AppCompatActivity implements 
        OnLoadCompleteListener, OnErrorListener {

    Drafts.Data template = new Drafts.Data();
    String TAG = "TAG";
    TextView textView;
    NestedScrollView nestedScrollView;
    ImageView back_btn;
    AlertDialog customDialog;
    SharedPreferences userPrefs;
    PDFView webView;
    TextView title, from, dateTextView, content, note;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_send);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        template = (Drafts.Data) getIntent().getSerializableExtra("template");

        title = (TextView) findViewById(R.id.title);
        from = (TextView) findViewById(R.id.from);
        dateTextView = (TextView) findViewById(R.id.date);
        note = (TextView) findViewById(R.id.sender_note);
        content = (TextView) findViewById(R.id.content);

        Button submitBtn = (Button) findViewById(R.id.btn_submit);
        textView = (TextView) findViewById(R.id.textView);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        webView = (PDFView) findViewById(R.id.webivew);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nested_scrollview);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new sendDraftApi().execute();
            }
        });

        if (template.getTemplateOption() == KeyConstants.TYPE_TEMPLATE) {
            title.setText(template.getTemplateNameEn());
            setTemplateData();
            textView.setVisibility(View.VISIBLE);
        }
        else {
            nestedScrollView.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            setPDFData();
        }

        note.setText("Note: "+template.getSenderNotes());
        from.setText("To: "+template.getFullName());
        String date = template.getDraftdate();
        date = date.replace("T", " ");
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date newDate = spf.parse(date);
            spf = new SimpleDateFormat("dd/MM/yyyy");
            date = spf.format(newDate);
            dateTextView.setText(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPDFData() {
        String jsonString = template.getSenderJSON();

        PDFData data = new PDFData();
        Gson gson = new Gson();
        data = gson.fromJson(jsonString, PDFData.class);

        title.setText(data.getDocumentName());

        Constants.Pinch.MINIMUM_ZOOM = 1f;
        Constants.Pinch.MAXIMUM_ZOOM = 1f;
        new showPDF().execute(com.cs.mawthuq.Utils.Constants.DOCUMENTS_URL + data.getDocumentName());

        if (template.getTemplateOption() == KeyConstants.TYPE_WRITE) {
            content.setVisibility(View.VISIBLE);
            content.setText("Content: "+data.getContent());
        }
    }

    @Override
    public void onError(Throwable t) {
        com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
        Toast.makeText(this, "Error displaying file", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadComplete(int nbPages) {
        com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
    }

    private class showPDF extends AsyncTask<String, Void, InputStream> {

        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream= null;
            try {
                URL uri = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) uri.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            com.cs.mawthuq.Utils.Constants.showLoadingDialog(DraftSendActivity.this);
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            super.onPostExecute(inputStream);
            webView.fromStream(inputStream)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(false)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .onLoad(DraftSendActivity.this)
                    .onError(DraftSendActivity.this)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    .spacing(0)
                    .autoSpacing(false) // add dynamic spacing to fit each page on its own on the screen
                    .pageFitPolicy(FitPolicy.BOTH) // mode to fit pages in the view
                    .fitEachPage(false) // fit each page to the view, else smaller pages are scaled relative to largest page.
                    .pageSnap(false) // snap pages to screen boundaries
                    .pageFling(false) // make a fling change only a single page like ViewPager
                    .nightMode(false) // toggle night mode
                    .load();
        }
    }


    private void setTemplateData() {
        String templateText = "";
        String jsonString = template.getSenderJSON(); //http request

        Log.d(TAG, "setTemplateData: "+jsonString);
        TemplateData data = new TemplateData();
        Gson gson = new Gson();
        data = gson.fromJson(jsonString, TemplateData.class);
        List<TemplateData.Blocks> blocks = data.getTemplate().getContent().get(0).getBlocks();
        for (int i = 0; i < blocks.size(); i++) {
            templateText = templateText + "\n" + blocks.get(i).getData().getText();
        }
        textView.setText(templateText);
    }

    private String prepareInsertJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("SenderId", userPrefs.getInt(KEY_USERID, 0));
            parentObj.put("DraftId", template.getDraftId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class sendDraftApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareInsertJson();
            com.cs.mawthuq.Utils.Constants.showLoadingDialog(DraftSendActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(DraftSendActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(com.cs.mawthuq.Utils.Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<BasicResponse> call = apiService.SendDraft(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    if (response.isSuccessful()) {
                        BasicResponse BasicResponse = response.body();
                        try {
                            if (BasicResponse.isStatus()) {
//                                Toast.makeText(DraftSendActivity.this, BasicResponse.MessageEn, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(DraftSendActivity.this, DashBordActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                com.cs.mawthuq.Utils.Constants.showOneButtonAlertDialogWithIntent(BasicResponse.MessageEn, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), DraftSendActivity.this, intent);
                            } else {
                                String failureResponse = BasicResponse.getMessageEn();
                                com.cs.mawthuq.Utils.Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), DraftSendActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(DraftSendActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(DraftSendActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(DraftSendActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(DraftSendActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    com.cs.mawthuq.Utils.Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}