package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.cs.mawthuq.Models.ValidateMobileResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_PASSWORD;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_PHONE;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERNAME;

public class SignUpActivity extends Activity {
    EditText inputname, inputmobile, inputPassword;
    Button createbtn;
    String strusername, strphone, strPassword;

    String TAG = "TAG";

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        inputname = (EditText) findViewById(R.id.edit_username);
        inputmobile = (EditText) findViewById(R.id.edit_phone);
        inputPassword = (EditText) findViewById(R.id.edit_password);
        createbtn = (Button) findViewById(R.id.btn_create);

        ImageView bak_btn = (ImageView) findViewById(R.id.bak_btn);
        bak_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        createbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validations()) {
                    new verifyMobileApi().execute();
                }
            }
        });

    }

    private boolean Validations() {
        strusername = inputname.getText().toString().trim();
        strphone = inputmobile.getText().toString().trim();
        strPassword = inputPassword.getText().toString().trim();

        if (strusername.length() == 0) {
            inputname.setError(getString(R.string.error_username));
            Constants.requestEditTextFocus(inputmobile, SignUpActivity.this);
            return false;
        }
        if (strphone.length() == 0) {
            inputmobile.setError(getString(R.string.error_phone));
            return false;
        }
        if (!strphone.startsWith("5")) {
            inputmobile.setError(getString(R.string.invalid_phone));
            return false;
        }
        if (strphone.length() < 9) {
            inputmobile.setError(getString(R.string.error_entervalidnumber));
            return false;
        }
        if (strPassword.length() < 5) {
            inputPassword.setError(getString(R.string.error_password));
            return false;
        }
        return true;
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("PhoneNumber", "966" + strphone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(SignUpActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<ValidateMobileResponse> call = apiService.validateMobile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ValidateMobileResponse>() {
                @Override
                public void onResponse(Call<ValidateMobileResponse> call, Response<ValidateMobileResponse> response) {
                    if (response.isSuccessful()) {
                        ValidateMobileResponse ValidateMobileResponse = response.body();
                        try {
                            if (ValidateMobileResponse.getStatus()) {
                                Log.d(TAG, "otp: " + ValidateMobileResponse.getData().getOtpCode());
                                Intent intent = new Intent(SignUpActivity.this, VerifyOtpActivity.class);
                                intent.putExtra(KEY_USERNAME, strusername);
                                intent.putExtra(KEY_PHONE, strphone);
                                intent.putExtra(KEY_PASSWORD, strPassword);
                                startActivity(intent);
                            } else {
                                String failureResponse = ValidateMobileResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), SignUpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<ValidateMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
