package com.cs.mawthuq.Activitys;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.mawthuq.Adapters.TemplateContactsAdapter;
import com.cs.mawthuq.Listeners.ContactSelectedListener;
import com.cs.mawthuq.MainActivity;
import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.github.drjacky.imagepicker.ImagePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class WriteANewActivity extends AppCompatActivity {

    File file;
    TextView documentName, add_contact;
    EditText etNote, etContent;
    Button nextBtn;
    ImageView back_btn;
    String TAG = "TAG";
    LinearLayout askSignatureLayout;
    SharedPreferences userPrefs;
    ViewPager contactsListView;
    TemplateContactsAdapter mAdapter;
    private ArrayList<Contacts.UserList> contacts = new ArrayList<>();
    private ContactSelectedListener listener;
    int selectedPos = -1;
    String encodeFileToBase64Binary;
    String inputStr;
    TextView saveDraft;
    private final int ADD_CONTACT = 1;

    String strFileName = "";
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_a_new);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        nextBtn = (Button) findViewById(R.id.btn_send_now);
        documentName = (TextView) findViewById(R.id.document_name);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        askSignatureLayout = (LinearLayout) findViewById(R.id.asksignaturelayout);
        contactsListView = (ViewPager) findViewById(R.id.contacts_list);
        etNote = (EditText) findViewById(R.id.sender_note);
        etContent = (EditText) findViewById(R.id.content);
        saveDraft = (TextView) findViewById(R.id.save);
        add_contact = (TextView) findViewById(R.id.add_contact);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        new getContactsListApi().execute();
        
        if (!canAccessStorage()) {
            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
        }

        add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WriteANewActivity.this, InviteContactActivity.class);
                intent.putExtra("contacts", contacts);
                startActivityForResult(intent, ADD_CONTACT);
            }
        });

        askSignatureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (canAccessStorage()) {
                        openCamera();
                    }
                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    }
                } else {
                    openCamera();
                }
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAndInsertData("send_now");
            }
        });

        saveDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAndInsertData("send_later");
            }
        });


        listener = new ContactSelectedListener() {
            @Override
            public void onContactSelected(int position) {
                selectedPos = position;
                mAdapter = new TemplateContactsAdapter(WriteANewActivity.this, contacts, listener, selectedPos);
                contactsListView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        };
    }

    private void validateAndInsertData(String type) {
        if (strFileName.equalsIgnoreCase("")) {
            Constants.showOneButtonAlertDialog("Please attach pdf file.",
                    getString(R.string.app_name), getString(R.string.ok), WriteANewActivity.this);
        }
        else if (selectedPos == -1) {
            Constants.showOneButtonAlertDialog("Please select contact", getResources().getString(R.string.app_name),
                    getResources().getString(R.string.ok), WriteANewActivity.this);
        }
        else if (etContent.getText().toString().trim().length() == 0) {
            etContent.setError("please enter content");
        }
        else if (etNote.getText().toString().trim().length() == 0) {
            etNote.setError("please enter note");
        }
        else {
            inputStr = prepareInsertJson(encodeFileToBase64Binary, strFileName);
            new insertSavedTemplateApi().execute(type);
        }
    }

    private void openCamera() {
        String[] mimeTypes = {"application/pdf"};

        ImagePicker.Companion.with(WriteANewActivity.this)
                .galleryOnly()
                .galleryMimeTypes(mimeTypes)
                .start();
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, perm));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_CONTACT) {
            new getContactsListApi().execute();
        }
        else if (resultCode == RESULT_OK) {
            String filePath = data.getStringExtra(ImagePicker.EXTRA_FILE_PATH);

            file = new File(filePath);
            documentName.setText(file.getName());
            encodeFileToBase64Binary = encodeFileToBase64Binary(file);

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
            strFileName = "NEW_" + timeStamp + ".pdf";
        }
    }

    private String encodeFileToBase64Binary(File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String encoded = Base64.encodeToString(bytes,Base64.DEFAULT);
        return encoded;
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (!canAccessStorage()) {
            Toast.makeText(this, getString(R.string.storage_permission_denied), Toast.LENGTH_SHORT).show();
        } else if (canAccessStorage()) {
            openCamera();
        }
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class getContactsListApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            Constants.showLoadingDialog(WriteANewActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(WriteANewActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<Contacts> call = apiService.getUserList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Contacts>() {
                @Override
                public void onResponse(Call<Contacts> call, Response<Contacts> response) {
                    if (response.isSuccessful()) {
                        Contacts Contacts = response.body();
                        Log.d(TAG, "onResponse: " + response.body());
                        try {
                            if (Contacts.getStatus()) {
                                contacts = Contacts.getData().getUserList();
                                mAdapter = new TemplateContactsAdapter(WriteANewActivity.this, contacts, listener, selectedPos);
                                contactsListView.setAdapter(mAdapter);
                            } else {
                                String failureResponse = Contacts.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), WriteANewActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(WriteANewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(WriteANewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<Contacts> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(WriteANewActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(WriteANewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareInsertJson(String encodeFileToBase64Binary, String strFileName) {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("SenderId", userPrefs.getInt(KEY_USERID, 0));
            parentObj.put("ReceiverId", contacts.get(selectedPos).getUserId());
            parentObj.put("TemplateId", 0);
            parentObj.put("TemplateType", Constants.DOCUMENT_OPTION);
            parentObj.put("TemplateOption", KeyConstants.TYPE_WRITE);
            parentObj.put("SendorNotes", etNote.getText().toString().trim());

            JSONObject senderObj = new JSONObject();
            senderObj.put("Content", etContent.getText().toString().trim());
            parentObj.put("SendorJSON", senderObj);

            JSONObject documentObj = new JSONObject();
            documentObj.put("FileName", strFileName);
            documentObj.put("Base64FileData", encodeFileToBase64Binary);
            documentObj.put("FileUploadLocation", "SignatureDocument");
            documentObj.put("FileURLLocation", "");
            parentObj.put("Document", documentObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class insertSavedTemplateApi extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(WriteANewActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(WriteANewActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(5).create(APIInterface.class);

            Call<BasicResponse> call = null;
            if (params[0].equalsIgnoreCase("send_now")) {
                call = apiService.insertSignatureDocument(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
            }
            else {
                call = apiService.saveDraft(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
            }

            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    if (response.isSuccessful()) {
                        BasicResponse BasicResponse = response.body();
                        try {
                            if (BasicResponse.isStatus()) {
                                Intent intent = new Intent(WriteANewActivity.this, DashBordActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                com.cs.mawthuq.Utils.Constants.showOneButtonAlertDialogWithIntent(BasicResponse.MessageEn, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), WriteANewActivity.this, intent);
                            } else {
                                String failureResponse = BasicResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), WriteANewActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(WriteANewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(WriteANewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(WriteANewActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(WriteANewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

}