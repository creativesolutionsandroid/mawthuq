package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.mawthuq.Adapters.ContactsListAdapter;
import com.cs.mawthuq.Adapters.TemplateContactsAdapter;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class ContactsActivity extends Activity {

    ImageView back_btn;
    EditText search;
    RecyclerView contactsList;
    LinearLayout addContactLayout;
    ContactsListAdapter mAdapter;
    private ArrayList<Contacts.UserList> contacts = new ArrayList<>();
    private ArrayList<Contacts.UserList> contactsFiltered = new ArrayList<>();
    SharedPreferences userPrefs;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact_activitty);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        search = (EditText) findViewById(R.id.search);
        back_btn = (ImageView) findViewById(R.id.bak_btn);
        addContactLayout = (LinearLayout) findViewById(R.id.layout_addcontact);
        contactsList = (RecyclerView) findViewById(R.id.contacts_list);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String searchText = editable.toString();
                contactsFiltered.clear();
                for (int i = 0; i < contacts.size(); i++) {
                    if (contacts.get(i).getFullName().toLowerCase().contains(searchText)||
                            contacts.get(i).getPhoneNumber().contains(searchText)) {
                        contactsFiltered.add(contacts.get(i));
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        search.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Constants.hideKeyboard(ContactsActivity.this);
                    return true;
                }
                return false;
            }
        });

        addContactLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactsActivity.this, InviteContactActivity.class);
                intent.putExtra("contacts", contacts);
                startActivity(intent);
            }
        });
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class getContactsListApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            contactsFiltered.clear();
            Constants.showLoadingDialog(ContactsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ContactsActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<Contacts> call = apiService.getUserList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Contacts>() {
                @Override
                public void onResponse(Call<Contacts> call, Response<Contacts> response) {
                    if (response.isSuccessful()) {
                        Contacts Contacts = response.body();
                        Log.d("TAG", "onResponse: " + response.body());
                        try {
                            if (Contacts.getStatus()) {
                                contacts = Contacts.getData().getUserList();
                                contactsFiltered.addAll(contacts);
                                mAdapter = new ContactsListAdapter(ContactsActivity.this, contactsFiltered);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ContactsActivity.this);
                                contactsList.setLayoutManager(new GridLayoutManager(ContactsActivity.this, 1));
                                contactsList.setAdapter(mAdapter);
                            } else {
                                TextView noContats = (TextView) findViewById(R.id.no_contacts_added);
                                noContats.setVisibility(View.VISIBLE);
                                String failureResponse = Contacts.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ContactsActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ContactsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ContactsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<Contacts> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ContactsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ContactsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new getContactsListApi().execute();
    }
}
