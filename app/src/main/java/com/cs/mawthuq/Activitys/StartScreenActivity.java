package com.cs.mawthuq.Activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.cs.mawthuq.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class StartScreenActivity extends Activity {
    TextView letstext;
    SharedPreferences prefs = null;



//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startscreen_activity);

        letstext=(TextView)findViewById(R.id.letsbegin);

        letstext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartScreenActivity.this,GetStartedScreen.class);
                startActivity(intent);
            }
        });

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        if (prefs.getBoolean("firstrun", true)) {
//            // Do first run stuff here then set 'firstrun' as false
//            //strat  DataActivity beacuase its your app first run
//            // using the following line to edit/commit prefs
//            prefs.edit().putBoolean("firstrun", false).commit();
//            startActivity(new Intent(HelperActivity.ths , SetupActivity.class));
//            finish();
//        }
//        else {
//            startActivity(new Intent(HelperActivity.ths , MainActivity.class));
//            finish();
//        }
//    }

}
