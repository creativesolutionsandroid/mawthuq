package com.cs.mawthuq.Activitys;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.mawthuq.Adapters.TemplateContactsAdapter;
import com.cs.mawthuq.Listeners.ContactSelectedListener;
import com.cs.mawthuq.MainActivity;
import com.cs.mawthuq.Models.BasicResponse;
import com.cs.mawthuq.Models.Contacts;
import com.cs.mawthuq.Models.TemplateData;
import com.cs.mawthuq.Models.TemplateList;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.KeyConstants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class SenderTemplateSubmitActivity extends AppCompatActivity {

    int selectedPos = -1;
    String TAG = "TAG";
    SharedPreferences userPrefs;
    ViewPager contactsListView;
    EditText etNote;
    Button btnSendNow;
    TextView saveDraft, add_contact;
    TemplateContactsAdapter mAdapter;
    private ArrayList<Contacts.UserList> contacts = new ArrayList<>();
    TemplateList.Template template = new TemplateList.Template();
    ImageView back_btn;
    private ContactSelectedListener listener;

    private final int ADD_CONTACT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender_template_submit);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        template = (TemplateList.Template) getIntent().getSerializableExtra("template");
        selectedPos = -1;

        contactsListView = (ViewPager) findViewById(R.id.contacts_list);
        etNote = (EditText) findViewById(R.id.sender_note);
        btnSendNow = (Button) findViewById(R.id.btn_send_now);
        saveDraft = (TextView) findViewById(R.id.save);
        add_contact = (TextView) findViewById(R.id.add_contact);
        back_btn = (ImageView) findViewById(R.id.bak_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        new getContactsListApi().execute();

        btnSendNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAndInsertData("send_now");
            }
        });

        saveDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAndInsertData("send_later");
            }
        });

        add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SenderTemplateSubmitActivity.this, InviteContactActivity.class);
                intent.putExtra("contacts", contacts);
                startActivityForResult(intent, ADD_CONTACT);
            }
        });

        listener = new ContactSelectedListener() {
            @Override
            public void onContactSelected(int position) {
                selectedPos = position;
                mAdapter = new TemplateContactsAdapter(SenderTemplateSubmitActivity.this, contacts
                        ,listener, selectedPos);
                contactsListView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        };
    }

    private void validateAndInsertData(String type) {
        if (selectedPos == -1) {
            Constants.showOneButtonAlertDialog("Please select contact", getResources().getString(R.string.app_name),
                    getResources().getString(R.string.ok), SenderTemplateSubmitActivity.this);
        }
        else if (etNote.getText().toString().trim().length() == 0) {
            etNote.setError("please enter note");
        }
        else {
            new insertSavedTemplateApi().execute(type);
        }
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class getContactsListApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            Constants.showLoadingDialog(SenderTemplateSubmitActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SenderTemplateSubmitActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<Contacts> call = apiService.getUserList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Contacts>() {
                @Override
                public void onResponse(Call<Contacts> call, Response<Contacts> response) {
                    if (response.isSuccessful()) {
                        Contacts Contacts = response.body();
                        Log.d(TAG, "onResponse: " + response.body());
                        try {
                            if (Contacts.getStatus()) {
                                contacts = Contacts.getData().getUserList();
                                mAdapter = new TemplateContactsAdapter(SenderTemplateSubmitActivity.this, contacts
                                ,listener, selectedPos);
                                contactsListView.setAdapter(mAdapter);
                            } else {
                                String failureResponse = Contacts.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), SenderTemplateSubmitActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SenderTemplateSubmitActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SenderTemplateSubmitActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<Contacts> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SenderTemplateSubmitActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SenderTemplateSubmitActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareInsertJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("SenderId", userPrefs.getInt(KEY_USERID, 0));
            parentObj.put("ReceiverId", contacts.get(selectedPos).getUserId());
            parentObj.put("TemplateId", template.getTemplateId());
            parentObj.put("TemplateType", template.getTemplateType());
            parentObj.put("TemplateOption", KeyConstants.TYPE_TEMPLATE);
            parentObj.put("SendorNotes", etNote.getText().toString().trim());

            JSONObject documentObj = new JSONObject();
            parentObj.put("Document", documentObj);

            String jsonString = template.getTemplatejsonEn();
            TemplateData data = new TemplateData();
            Gson gson = new Gson();
            data = gson.fromJson(jsonString, TemplateData.class);
            List<TemplateData.ControlsArray> controls = data.getControls().getControls();
            for (int i = 0; i < controls.size(); i++) {
                controls.get(i).setShoartCode("{{" + controls.get(i).getControlName() + "}}");
            }
            data.getControls().setControls(controls);
            String json = gson.toJson(data);
            try {
                JSONObject obj = new JSONObject(json);
                parentObj.put("SendorJSON", obj);
            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class insertSavedTemplateApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareInsertJson();
            Constants.showLoadingDialog(SenderTemplateSubmitActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SenderTemplateSubmitActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<BasicResponse> call = null;
            if (params[0].equalsIgnoreCase("send_now")) {
                call = apiService.insertSignatureDocument(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
            }
            else {
                call = apiService.saveDraft(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
            }

            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    if (response.isSuccessful()) {
                        BasicResponse BasicResponse = response.body();
                        try {
                            if (BasicResponse.isStatus()) {
                                Intent intent = new Intent(SenderTemplateSubmitActivity.this, DashBordActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                com.cs.mawthuq.Utils.Constants.showOneButtonAlertDialogWithIntent(BasicResponse.MessageEn, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), SenderTemplateSubmitActivity.this, intent);
                            } else {
                                String failureResponse = BasicResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), SenderTemplateSubmitActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SenderTemplateSubmitActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SenderTemplateSubmitActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SenderTemplateSubmitActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SenderTemplateSubmitActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_CONTACT) {
            new getContactsListApi().execute();
        }
    }
}