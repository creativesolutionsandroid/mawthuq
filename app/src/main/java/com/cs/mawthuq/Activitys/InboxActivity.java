package com.cs.mawthuq.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.mawthuq.Adapters.InboxAdapter;
import com.cs.mawthuq.Adapters.InboxViewPagerAdapter;
import com.cs.mawthuq.Adapters.TemplatesListAdapter;
import com.cs.mawthuq.Fragment.InboxLetterFragment;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.Models.InboxResponse;
import com.cs.mawthuq.R;
import com.cs.mawthuq.Rest.APIInterface;
import com.cs.mawthuq.Rest.ApiClient;
import com.cs.mawthuq.Utils.Constants;
import com.cs.mawthuq.Utils.NetworkUtil;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.mawthuq.Utils.KeyConstants.ID_LETTER;
import static com.cs.mawthuq.Utils.KeyConstants.KEY_USERID;

public class InboxActivity extends AppCompatActivity {

    ImageView back_btn;
    RecyclerView inboxListView;
    InboxAdapter mAdapter;
    private ArrayList<InboxResponse.Data> templates = new ArrayList<>();
    private ArrayList<InboxResponse.Data> letters = new ArrayList<>();
    private ArrayList<InboxResponse.Data> contracts = new ArrayList<>();
    String TAG = "TAG";
    SharedPreferences userPrefs;
    TabLayout tabs;
    ViewPager mViewPager;
    InboxViewPagerAdapter adapter;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        back_btn = (ImageView) findViewById(R.id.bak_btn);
        inboxListView = (RecyclerView) findViewById(R.id.template_list);
        tabs = (TabLayout) findViewById(R.id.tabs);
        mViewPager = (ViewPager) findViewById(R.id.container);

        setViewPager();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        new getInboxResponseApi().execute();
    }

    private String prepareSignInJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("TemplateId", "0");
            parentObj.put("UserId", userPrefs.getInt(KEY_USERID, 0));
            parentObj.put("UserType", 1);
            parentObj.put("DocumentStatus", 1);
            parentObj.put("PageSize", 100);
            parentObj.put("PageNumber", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }

    private class getInboxResponseApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignInJson();
            Constants.showLoadingDialog(InboxActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(InboxActivity.this);
            APIInterface apiService =
                    ApiClient.getClient(Constants.CONNECTION_TIMEOUT).create(APIInterface.class);

            Call<InboxResponse> call = apiService.GetSignatureDocument(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InboxResponse>() {
                @Override
                public void onResponse(Call<InboxResponse> call, Response<InboxResponse> response) {
                    if (response.isSuccessful()) {
                        InboxResponse InboxResponse = response.body();
                        Log.d(TAG, "onResponse: " + response.body());
                        try {
                            if (InboxResponse.getStatus()) {
                                templates = InboxResponse.getData();

                                filterData();
//                                mAdapter = new InboxAdapter(InboxActivity.this, templates);
//                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InboxActivity.this);
//                                inboxListView.setLayoutManager(new GridLayoutManager(InboxActivity.this, 1));
//                                inboxListView.setAdapter(mAdapter);
                            } else {
                                String failureResponse = InboxResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), InboxActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(InboxActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(InboxActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<InboxResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(InboxActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(InboxActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void filterData() {
        for (int i = 0; i < templates.size(); i++) {
            if (templates.get(i).getTemplateType() == ID_LETTER) {
                letters.add(templates.get(i));
            }
            else {
                contracts.add(templates.get(i));
            }
        }
        setViewPager();
    }

    private void setViewPager() {

        Constants.closeLoadingDialog();
        adapter = new InboxViewPagerAdapter(getSupportFragmentManager());

        InboxLetterFragment letterFragment = new InboxLetterFragment();
        Bundle letterBundle = new Bundle();
        letterBundle.putSerializable("inbox",letters);
        letterFragment.setArguments(letterBundle);

        InboxLetterFragment contractFragment = new InboxLetterFragment();
        Bundle contractBundle = new Bundle();
        contractBundle.putSerializable("inbox",contracts);
        contractFragment.setArguments(contractBundle);

        adapter.addFragment(letterFragment, "Letters");
        adapter.addFragment(contractFragment, "Contracts");

        mViewPager.setAdapter(adapter);
        tabs.setupWithViewPager(mViewPager);

    }
}